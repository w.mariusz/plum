#########################
[plum] Pack/Unpack Memory
#########################

.. image:: https://gitlab.com/dangass/plum/badges/master/pipeline.svg
    :target: https://gitlab.com/dangass/plum/commits/master
    :alt: Pipeline Status

.. image:: https://gitlab.com/dangass/plum/badges/master/coverage.svg
    :target: https://gitlab.com/dangass/plum/commits/master
    :alt: Coverage Report

.. image:: https://readthedocs.org/projects/plum-py/badge/?version=latest
    :target: https://plum-py.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

The |plum-py| Python package provides classes and utility functions to
transform byte sequences into Python objects and back. While similar in
purpose to Python's standard library :mod:`struct` module, this package
provides a larger set of format specifiers and is extensible, allowing
you to easily create complex ones of your own.
