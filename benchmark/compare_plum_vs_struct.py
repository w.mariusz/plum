# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Compare plum pack/unpack with plum vs. struct module."""

from struct import Struct

from benchmark import Measurements, PlumMeasurements

from plum import pack, unpack
from plum.int import IntX
from plum.structure import Structure, member

uint8 = IntX("uint8", nbytes=1)
uint16 = IntX("uint16", nbytes=2)
uint32 = IntX("uint32", nbytes=4)

INT_BYTES = b"\x01\x02\x03\x04"
INT_VALUE = 0x04030201

TUPLE_BYTES = b"\x01\x02\x03\x04" + b"\x01\x02" + b"\x01\x02" + b"\x01"
TUPLE_VALUE = (0x04030201, 0x0201, 0x0201, 0x01)


class StructInt(Measurements):

    fmt = Struct("<L")

    def time_unpack(self, buffer=INT_BYTES, fmt=fmt):
        value = fmt.unpack(buffer)[0]

    def time_pack(self, value=INT_VALUE, fmt=fmt):
        buffer = fmt.pack(value)


class StructTuple(Measurements):

    tuple_fmt = Struct("<LHHB")

    def time_unpack(self, buffer=TUPLE_BYTES, fmt=tuple_fmt):
        value = fmt.unpack(buffer)[0]

    def time_pack(self, value=TUPLE_VALUE, fmt=tuple_fmt):
        buffer = fmt.pack(*value)


class PlumInt(PlumMeasurements):
    def time_unpack(self, buffer=INT_BYTES, fmt=uint32, op=unpack):
        value = op(fmt, buffer)

    def time_pack(self, value=INT_VALUE, fmt=uint32, op=pack):
        buffer = op(fmt, value)


class PlumTuple(PlumMeasurements):

    tuple_fmt = (uint32, uint16, uint16, uint8)

    def time_unpack(self, buffer=TUPLE_BYTES, fmt=tuple_fmt, op=unpack):
        value = op(fmt, buffer)

    def time_pack(self, value=TUPLE_VALUE, fmt=tuple_fmt, op=pack):
        buffer = op(fmt, *value)


class Sample(Structure):

    member1: int = member(fmt=uint32)
    member2: int = member(fmt=uint16)
    member3: int = member(fmt=uint16)
    member4: int = member(fmt=uint8)


class PlumStruct(PlumMeasurements):
    def time_unpack(self, buffer=TUPLE_BYTES, fmt=Sample, op=unpack):
        value = op(fmt, buffer)

    def time_pack(self, value=TUPLE_VALUE, fmt=Sample, op=pack):
        buffer = op(fmt, value)


if __name__ == "__main__":

    if PlumMeasurements.run_me(__file__):
        struct_int = StructInt()
        plum_int = PlumInt()
        PlumMeasurements.compare(struct_int, plum_int)

        struct_tuple = StructTuple()
        plum_tuple = PlumTuple()
        plum_struct = PlumStruct()
        PlumMeasurements.compare(struct_tuple, plum_tuple, plum_struct)

        plum_int.dump()
        plum_tuple.dump()
        plum_struct.dump()
