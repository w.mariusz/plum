# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Facility to measure execution times of various operations."""

import json
import os
import sys
import timeit
from datetime import date

from texttable import Texttable


PLUM_BOOST = os.environ.get("ENABLE_PLUM_BOOST", None) == "YES"


class Measurements(dict):

    """Execution time measurements."""

    def adjust_label(self, label):
        return label

    def __init__(self, label=None, times=None):
        if label is None:
            label = self.adjust_label(type(self).__name__)

        self["label"] = label

        if times is None:
            self["times"] = {"do_nothing": 0}
            self.measure("do_nothing", self.do_nothing)

            for item_name in dir(self):
                if item_name.startswith("time_"):
                    time_function = getattr(self, item_name)
                    measurement_name = item_name[len("time_") :]
                    self.measure(measurement_name, time_function)

        else:
            self["times"] = times

    @staticmethod
    def do_nothing():
        """Do nothing, just return."""

    def measure(self, name, method):
        """Measure execution time of a function.

        :param str name:
        :param callable method: function to measure

        """
        total_duration = (
            min(timeit.repeat(method, number=10, repeat=3000)) * 1000000000 / 10
        )

        self["times"][name] = total_duration - self["times"]["do_nothing"]

    def dump(self):
        script = os.environ.get("BENCHMARK_SCRIPT", "anonymous.py")
        filepath = (
            os.path.join(
                "measurements",
                date.today().strftime("%Y_%m_%d"),
                os.path.splitext(script)[0],
                self["label"]
                .replace(" ", "_")
                .replace("(", "")
                .replace(")", "")
                .lower(),
            )
            + ".json"
        )

        dirpath = os.path.dirname(filepath)

        if not os.path.exists(dirpath):
            os.makedirs(dirpath)

        with open(filepath, "w") as fh:
            json.dump(self, fh, indent=2)

    @classmethod
    def load(cls, filename):
        with open(filename) as fh:
            measurements = json.load(fh)
        return Measurements(measurements["label"], measurements["times"])

    @staticmethod
    def compare(*measurements):
        names = set(measurements[0]["times"])

        for m in measurements[1:]:
            names = names & set(m["times"])

        rows = [[""] + [m["label"] for m in measurements]]

        for name in sorted(names - set(["do_nothing"])):
            times = [m["times"][name] for m in measurements]
            minval = min(times)
            rows += [
                [name] + [f"{int(time)}ns  ({time / minval:.1f}X)" for time in times]
            ]

        table = Texttable()
        table.set_cols_dtype(["t"] + (["a"] * len(measurements)))
        table.set_cols_align(["l"] + (["r"] * len(measurements)))
        table.add_rows(rows)
        print(table.draw())
        print()

    @staticmethod
    def run_me(scriptname, boost=("YES", "NO")):
        benchmark_name = os.environ.get("BENCHMARK_SCRIPT", None)

        if benchmark_name is None:
            os.environ["BENCHMARK_SCRIPT"] = os.path.basename(scriptname)
            for setting in boost:
                os.environ["ENABLE_PLUM_BOOST"] = setting
                os.system(f'{sys.executable} {scriptname} --boost="{setting}"')

        return benchmark_name is not None


class PlumMeasurements(Measurements):

    """Plum execution time measurements."""

    def adjust_label(self, label):
        return label + (" (boost)" if PLUM_BOOST else " (no boost)")
