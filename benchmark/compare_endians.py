# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Compare plum big endian and little endian integer unpacking."""

from benchmark import PlumMeasurements

from plum.int import IntX
from plum.utilities import pack, unpack

uint32big = IntX("uint32big", nbytes=4, byteorder="big")
uint32little = IntX("uint32little", nbytes=4)

BE_BYTES = b"\x04\x03\x02\x01"
LE_BYTES = b"\x01\x02\x03\x04"
VALUE = 0x01020304


class Big(PlumMeasurements):

    """Big Endian"""

    @staticmethod
    def time_unpack(buffer=BE_BYTES):
        unpack(uint32big, buffer)

    @staticmethod
    def time_pack(value=VALUE):
        pack(value, uint32big)


class Little(PlumMeasurements):

    """Little Endian"""

    @staticmethod
    def time_unpack(buffer=LE_BYTES):
        unpack(uint32little, buffer)

    @staticmethod
    def time_pack(value=VALUE):
        pack(value, uint32little)


if __name__ == "__main__":

    if PlumMeasurements.run_me(__file__):
        big = Big()
        little = Little()

        PlumMeasurements.compare(big, little)
