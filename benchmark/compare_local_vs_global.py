# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Compare plum pack/unpack with plum vs. struct module."""

from struct import Struct

from benchmark import Measurements, PlumMeasurements

from plum import pack, unpack, pack_and_dump, unpack_and_dump
from plum.int import IntX

uint32 = IntX("uint32", nbytes=4)

LE_INT_BYTES = b"\x01\x02\x03\x04"
VALUE = 0x01020304


class Local(PlumMeasurements):
    def time_unpack_int(self, buffer=LE_INT_BYTES, fmt=uint32, op=unpack):
        value = op(fmt, buffer)

    def time_pack_int(self, value=VALUE, fmt=uint32, op=pack):
        buffer = op(fmt, value)


class Global(PlumMeasurements):
    def time_unpack_int(self, buffer=LE_INT_BYTES):
        value = unpack(uint32, buffer)

    def time_pack_int(self, value=VALUE):
        buffer = pack(value, uint32)


if __name__ == "__main__":

    if PlumMeasurements.run_me(__file__):
        local = Local()
        globs = Global()

        PlumMeasurements.compare(local, globs)
