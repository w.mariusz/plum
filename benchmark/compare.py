# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Runs every comparison script."""

import sys
from glob import glob
from subprocess import check_output

for name in glob("compare_*.py"):
    print("=" * 80)
    print(f" {name} ".center(80, "="))
    print("=" * 80)
    print()
    print(check_output([sys.executable, name]).decode("ascii"))
