# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Measure effectiveness of boost of __pack__ and __unpack__."""

from benchmark import Measurements, PlumMeasurements

from plum import pack, unpack
from plum.int import IntX
from plum.array import Array
from plum.structure import Structure, dimmed_member, member

uint32 = IntX("uint32", nbytes=4)


class ArrayItem(Structure):

    member0: int = member(fmt=uint32)
    member1: int = member(fmt=uint32)
    member2: int = member(fmt=uint32)
    member3: int = member(fmt=uint32)
    member4: int = member(fmt=uint32)
    member5: int = member(fmt=uint32)
    member6: int = member(fmt=uint32)
    member7: int = member(fmt=uint32)
    member8: int = member(fmt=uint32)
    member9: int = member(fmt=uint32)


class SampleArray(Array, item_cls=ArrayItem):

    pass


class Sample(Structure):

    nitems: int = member(fmt=uint32, compute=True)
    array: list = dimmed_member(dims=nitems, fmt=SampleArray)


BYTES = b"\x0a\x00\x00\x00" + (b"\x00" * 10 * 10 * 4)
VALUE = {"array": [[0] * 10] * 10}


class PlumStruct(PlumMeasurements):
    def time_unpack(self, buffer=BYTES, fmt=Sample, op=unpack):
        value = op(fmt, buffer)

    def time_pack(self, value=VALUE, fmt=Sample, op=pack):
        buffer = op(fmt, value)


if __name__ == "__main__":

    if PlumMeasurements.run_me(__file__):
        sample_measurements = PlumStruct()
        PlumMeasurements.compare(sample_measurements)

        sample_measurements.dump()
