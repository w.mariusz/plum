# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Compare plum pack/unpack with plum vs. struct module."""

import os
from struct import Struct

from benchmark import Measurements, PlumMeasurements

from plum import pack, unpack, pack_and_dump, unpack_and_dump
from plum.int import IntX

uint8 = IntX("uint8", nbytes=1)
uint16 = IntX("uint16", nbytes=2)
uint32 = IntX("uint32", nbytes=4)

BYTES = b"\x01\x02\x03\x04" + b"\x01\x02" + b"\x01\x02" + b"\x01"
VALUE = (0x04030201, 0x0201, 0x0201, 0x01)
FMT = (uint32, uint16, uint16, uint8)


class NoDump(PlumMeasurements):
    def time_unpack(self, buffer=BYTES, fmt=FMT, op=unpack):
        value = op(fmt, buffer)

    def time_pack(self, value=VALUE, fmt=FMT, op=pack):
        buffer = op(fmt, *value)


class Dump(PlumMeasurements):
    def time_unpack(self, buffer=BYTES, fmt=FMT, op=unpack_and_dump):
        value = op(fmt, buffer)

    def time_pack(self, value=VALUE, fmt=FMT, op=pack_and_dump):
        buffer = op(fmt, *value)


if __name__ == "__main__":

    if PlumMeasurements.run_me(__file__, boost=["YES"]):
        plum_no_dump = NoDump()
        plum_dump = Dump()

        PlumMeasurements.compare(plum_no_dump, plum_dump)

        plum_no_dump.dump()
        plum_dump.dump()
