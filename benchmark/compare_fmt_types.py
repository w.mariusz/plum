# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Compare plum pack/unpack with list format vs. tuple format."""

from benchmark import PlumMeasurements

from plum import pack, unpack
from plum.int import IntX
from plum.structure import Structure, member

uint32 = IntX("uint32", nbytes=4)

LE_BYTES = b"\x01\x02\x03\x04"

VALUE = 0x01020304


class Tuple(PlumMeasurements):
    def time_pack(self, fmt=(uint32, uint32), value=VALUE):
        buffer = pack((value, value), fmt)

    def time_unpack(self, fmt=(uint32, uint32), buffer=LE_BYTES * 2):
        value = unpack(fmt, buffer)[0]


class List(PlumMeasurements):
    def time_pack(self, fmt=[uint32, uint32], value=VALUE):
        buffer = pack((value, value), fmt)

    def time_unpack(self, fmt=[uint32, uint32], buffer=LE_BYTES * 2):
        value = unpack(fmt, buffer)[0]


class Dict(PlumMeasurements):

    fmt = {"a": uint32, "b": uint32}

    def time_pack(self, fmt=fmt, value=VALUE):
        buffer = pack({"a": value, "b": value}, fmt)

    def time_unpack(self, fmt=fmt, buffer=LE_BYTES * 2):
        value = unpack(fmt, buffer)["a"]


class Sample(Structure):

    member1: int = member(fmt=uint32)
    member2: int = member(fmt=uint32)


class Indexed(PlumMeasurements):
    def time_pack(self, fmt=Sample, value=[VALUE, VALUE]):
        buffer = pack(value, fmt)

    def time_unpack(self, fmt=Sample, buffer=LE_BYTES * 2):
        value = unpack(fmt, buffer)[0]


class Named(PlumMeasurements):
    def time_pack(self, fmt=Sample, value=VALUE):
        buffer = pack(fmt(member1=value, member2=value), fmt)

    def time_unpack(self, fmt=Sample, buffer=LE_BYTES * 2):
        value = unpack(fmt, buffer).member1


if __name__ == "__main__":

    if PlumMeasurements.run_me(__file__):
        tup = Tuple()
        lst = List()
        dct = Dict()
        indexed = Indexed()
        named = Named()

        PlumMeasurements.compare(tup, lst, dct, indexed, named)
