# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Compare plum pack/unpack with plum vs. struct module."""

from benchmark import Measurements, PlumMeasurements

bytes_not_available = MemoryError(
    "buffer does not contain enough bytes for the request"
)


class Buffer:
    def __init__(self, offset):
        self.offset = offset
        self.buffer = bytearray(10)

    def consume_n(self, nbytes):
        data = self.buffer[self.offset : self.offset + nbytes]

        if len(data) < nbytes:
            return None

        return data

    def consume_b(self, nbytes):
        data = self.buffer[self.offset : self.offset + nbytes]

        if len(data) < nbytes:
            return b""

        return data

    def consume_a(self, nbytes):
        data = self.buffer[self.offset : self.offset + nbytes]

        if len(data) < nbytes:
            return bytearray()

        return data


class ReturnBytes(PlumMeasurements):
    def time_it(self, buffer=Buffer(offset=5)):
        data = buffer.consume_b(7)
        if data is not None:
            data += b"something to do"


class ReturnByteArray(PlumMeasurements):
    def time_it(self, buffer=Buffer(offset=5)):
        data = buffer.consume_a(7)
        if data is not None:
            data += b"something to do"


class ReturnNone(PlumMeasurements):
    def time_it(self, buffer=Buffer(offset=5)):
        data = buffer.consume_n(7)
        if data is not None:
            data += b"something to do"


if __name__ == "__main__":

    if PlumMeasurements.run_me(__file__):
        ra = ReturnByteArray()
        rb = ReturnBytes()
        rn = ReturnNone()

        PlumMeasurements.compare(ra, rb, rn)
