# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Test reference count handling in boost unpack() function."""

import sys

from baseline import Baseline

from plum import boost
from plum.data import Plum, PlumMeta
from plum.exceptions import UnpackError
from plum.int import IntX
from plum.utilities import getbytes, unpack

from .utils import RefCounts as RefCountsBase

uint8 = IntX("uint8", nbytes=1)


class CustomError(Plum, metaclass=PlumMeta):  # pylint: disable=abstract-method

    """Always raises ValueError when unpacked."""

    @classmethod
    def __unpack__(cls, buffer, offset, parent, dump):
        if dump:
            dump.value = "<invalid>"
            getbytes(buffer, offset, dump, 1)
        raise ValueError("invalid byte")


buf00 = b"\x00"


class RefCounts(RefCountsBase):

    """Python object reference count tracker."""

    def __init__(self):
        super().__init__(
            self,
            uint8,
            buf00=buf00,
        )


def test_one_pos_arg():
    """Test only one positional argument (fmt).

    branches: UNPACK:POS_FAIL

    """
    if boost:
        expect_message = Baseline(
            """
            unpack expected 2 arguments, got 1
            """
        )
    else:
        expect_message = Baseline(
            """
            unpack() missing 1 required positional argument: 'buffer'
            """
        )

    expect = RefCounts()
    try:
        unpack(uint8)
    except TypeError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_three_pos_arg():
    """Test excess positional arguments.

    branches: UNPACK:POS_FAIL

    """
    if boost:
        expect_message = Baseline(
            """
            unpack expected 2 arguments, got 3
            """
        )
    else:
        expect_message = Baseline(
            """
            unpack() takes 2 positional arguments but 3 were given
            """
        )

    expect = RefCounts()
    try:
        unpack(uint8, 0, 1)
    except TypeError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_error():
    """Test unpack error.

    branches: UNPACK:ERROR

    """
    expect_message = Baseline(
        """


        +--------+-----------+-------+-------------+
        | Offset | Value     | Bytes | Format      |
        +--------+-----------+-------+-------------+
        | 0      | <invalid> | 00    | CustomError |
        +--------+-----------+-------+-------------+

        ValueError occurred during unpack operation:

        invalid byte
        """
    )

    expect = RefCounts()
    try:
        unpack(CustomError, buf00)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_happy_path():
    """Test unpack, no error.

    branches: UNPACK

    """
    # interesting fact - small integers produced by int.from_bytes( ) are unique
    # and not the same objects as Python's cache of small integers.
    expect = RefCounts()
    retval = unpack(uint8, buf00)
    actual = RefCounts()

    assert sys.getrefcount(retval) == 2
    assert actual == expect
    assert retval == 0
