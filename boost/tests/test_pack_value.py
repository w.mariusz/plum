# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Test reference count handling in boost pack_value() function."""

from baseline import Baseline

from plum.exceptions import PackError
from plum.utilities import pack
from plum.int import IntX
from plum.str import AsciiStr

AsciiStr = StrX("ascii", encoding="ascii")
uint8 = IntX("uint8", nbytes=1, byteorder="big")
uint16 = IntX("uint16", nbytes=2, byteorder="big")

from .utils import RefCounts as RefCountsBase

shared_memory = b"\x02\x01\x00"
uint8_good_view = uint8.view(shared_memory, offset=1)
uint8_bad_view = uint8.view(shared_memory, offset=3)
big_int = 0x0708

fmt_int_tuple = (uint8, uint16)
fmt_str_tuple = (AsciiStr, AsciiStr)

good_str = "hello"
bad_str = chr(1000)


class RefCounts(RefCountsBase):

    """Python object reference count tracker."""

    def __init__(self):
        super().__init__(
            self,
            AsciiStr,
            uint8,
            uint16,
            shared_memory=shared_memory,
            uint8_bad_view=uint8_bad_view,
            uint8_good_view=uint8_good_view,
            fmt_int_tuple=fmt_int_tuple,
            good_str=good_str,
            bad_str=bad_str,
        )


def test_value_is_view_success():
    """Test value is view and get is successful.

    branches: PACK_VAL:VAL_IS_VIEW:GET_SUCCESS

    """

    expect = RefCounts()
    retval = pack(uint8, uint8_good_view)
    actual = RefCounts()

    assert retval == b"\x01"
    assert actual == expect


def test_value_is_view_fail():
    """Test value is view but get fails.

    branches: PACK_VAL:VAL_IS_VIEW:GET_FAIL

    """
    exp_message = Baseline(
        """


        +--------+---------------+-------+-------+
        | Offset | Value         | Bytes | Format|
        +--------+---------------+-------+-------+
        |        | <view at 0x3> |       | uint8 |
        +--------+---------------+-------+-------+

        UnpackError occurred during pack operation:



        +--------+----------------------+-------+-------+
        | Offset | Value                | Bytes | Format|
        +--------+----------------------+-------+-------+
        |        | <insufficient bytes> |       | uint8 |
        +--------+----------------------+-------+-------+

        InsufficientMemoryError occurred during unpack operation:

        1 too few bytes to unpack uint8 (1 needed, only 0 available)
        """
    )

    expect = RefCounts()
    try:
        pack(uint8, uint8_bad_view)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == exp_message
    assert actual == expect


def test_fast_and_fixed_create():
    """Test packing with boost and fixed size and creating the buffer.

    branches: PACK_VAL:FASTDATA_AND_FIXED:BUF_CREATE
    branches: PACK_VAL:FASTDATA_AND_FIXED:FAST_PACK

    """

    expect = RefCounts()
    retval = pack(uint16, big_int)
    actual = RefCounts()

    assert retval == b"\x07\x08"
    assert actual == expect


def test_fast_and_fixed_resizing():
    """Test packing with boost and fixed size and resizing the buffer.

    branches: PACK_VAL:FASTDATA_AND_FIXED:BUF_EXIST:RESIZING
    branches: PACK_VAL:FASTDATA_AND_FIXED:FAST_PACK

    """

    expect = RefCounts()
    retval = pack(fmt_int_tuple, 0, big_int)
    actual = RefCounts()

    assert retval == b"\x00\x07\x08"
    assert actual == expect


def test_slow_create():
    """Test packing without boost and creating the buffer.

    branches: PACK_VAL:SLOW_OR_VAR:BUF_CREATE
    branches: PACK_VAL:SLOW_OR_VAR:SLOW_PACK
    branches: PACK_VAL:SLOW_OR_VAR:SLOW_PACK:SUCCESS

    """

    expect = RefCounts()
    retval = pack(AsciiStr, good_str)
    actual = RefCounts()

    assert retval == b"hello"
    assert actual == expect


def test_slow_fail():
    """Test packing without boost and an exception.

    branches: PACK_VAL:SLOW_OR_VAR:SLOW_PACK:FAIL

    """
    exp_message = Baseline(
        """


        +--------+-----------+-------+-------+----------+
        | Offset | Access    | Value | Bytes | Format   |
        +--------+-----------+-------+-------+----------+
        |        |           |       |       | AsciiStr |
        |        | [0:0]     | ''    |       |          |
        |        | --error-- | '\u03e8'   |       |          |
        +--------+-----------+-------+-------+----------+

        UnicodeEncodeError occurred during pack operation:

        'ascii' codec can't encode character '\\u03e8' in position 0: ordinal not in range(128)
        """
    )

    expect = RefCounts()
    try:
        pack(AsciiStr, bad_str)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == exp_message
    assert actual == expect
