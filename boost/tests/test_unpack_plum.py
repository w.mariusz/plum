# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Test reference count handling in boost unpack_plum() function."""

from baseline import Baseline

from plum.data import Plum, PlumMeta
from plum.exceptions import UnpackError
from plum.utilities import unpack
from plum.int import IntX
from plum.bytearray import ByteArray

from .utils import RefCounts as RefCountsBase

uint8 = IntX("uint8", nbytes=1)


class OffsetBad(Plum, metaclass=PlumMeta):  # pylint: disable=abstract-method

    """Returns invalid offset."""

    @classmethod
    def __unpack__(cls, buffer, offset, parent, dump):
        # pylint: disable=unused-argument
        return 0, "hi"


class NotTuple(Plum, metaclass=PlumMeta):  # pylint: disable=abstract-method

    """Returns something that isn't a tuple."""

    @classmethod
    def __unpack__(cls, buffer, offset, parent, dump):
        # pylint: disable=unused-argument
        return 0


class TupleWrongLen(Plum, metaclass=PlumMeta):  # pylint: disable=abstract-method

    """Return tuple of length 3 (instead of 2)."""

    @classmethod
    def __unpack__(cls, buffer, offset, parent, dump):
        # pylint: disable=unused-argument
        return 1, 1, 1


buf00 = b"\x00"
buf0001 = b"\x00\x01"


class ByteArray1(ByteArray, nbytes=1):

    """Fixed size byte array."""


class RefCounts(RefCountsBase):

    """Python object reference count tracker."""

    def __init__(self):
        super().__init__(
            self,
            uint8,
            buf00=buf00,
            bytearray1_cls=ByteArray1,
            offsetbad_cls=OffsetBad,
            not_tuple_cls=NotTuple,
            tuple_wrong_len_cls=TupleWrongLen,
        )


def test_fast():
    """Test unpacking with fast method.

    branches: UNPACK_PLUM:FAST

    """
    expect = RefCounts()
    retval = unpack(uint8, buf00)
    actual = RefCounts()

    assert retval == 0
    assert actual == expect


def test_slow_happy_path():
    """Test unpacking with slow method with success.

    branches: UNPACK_PLUM:SLOW:SUCCEED:TUPLE_OK:OFFSET_OK

    """
    expect = RefCounts()
    retval = unpack(ByteArray1, buf00)
    actual = RefCounts()

    assert retval == buf00
    assert actual == expect


def test_slow_bad_offset():
    """Test unpacking with slow method and bad offset returned.

    branches: UNPACK_PLUM:SLOW:SUCCEED:TUPLE_OK:OFFSET_BAD

    """
    expect_message = Baseline(
        """


        +--------+--------+-------+-------+-----------+
        | Offset | Access | Value | Bytes | Format    |
        +--------+--------+-------+-------+-----------+
        |        | [0]    |       |       | OffsetBad |
        |        | [1]    |       |       | uint8     |
        +--------+--------+-------+-------+-----------+

        TypeError occurred during unpack operation:

        can only concatenate str (not "int") to str
        """
    )

    expect = RefCounts()
    try:
        unpack([OffsetBad, uint8], buf0001)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    # Py3.6 exception message slightly different
    exc_message = exc_message.replace(
        "must be str, not int", 'can only concatenate str (not "int") to str'
    )

    assert exc_message == expect_message
    assert actual == expect


def test_slow_fail():
    """Test unpacking with slow method and failure.

    branches: UNPACK_PLUM:SLOW:ERROR

    """
    expect_message = Baseline(
        """


        +--------+----------------------+-------+------------+
        | Offset | Value                | Bytes | Format     |
        +--------+----------------------+-------+------------+
        |        | <insufficient bytes> |       | ByteArray1 |
        +--------+----------------------+-------+------------+

        InsufficientMemoryError occurred during unpack operation:

        1 too few bytes to unpack ByteArray1 (1 needed, only 0 available)
        """
    )

    expect = RefCounts()
    try:
        unpack_from(ByteArray1, buf00, 3)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_slow_not_a_tuple():
    """Test unpacking with slow method and return type failure.

    branches: UNPACK_PLUM:SLOW:TUPLE_BAD

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+----------+
        | Offset | Value | Bytes | Format   |
        +--------+-------+-------+----------+
        |        |       |       | NotTuple |
        +--------+-------+-------+----------+

        TypeError occurred during unpack operation:

        cannot unpack non-iterable int object
        """
    )

    expect = RefCounts()
    try:
        unpack(NotTuple, buf00)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    # Py3.6 exception message slightly different
    exc_message = exc_message.replace(
        "'int' object is not iterable", "cannot unpack non-iterable int object"
    )

    assert exc_message == expect_message
    assert actual == expect


def test_slow_tuple_bad():
    """Test unpacking with slow method and return tuple wrong length.

    branches: UNPACK_PLUM:SLOW:TUPLE_BAD

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+---------------+
        | Offset | Value | Bytes | Format        |
        +--------+-------+-------+---------------+
        |        |       |       | TupleWrongLen |
        +--------+-------+-------+---------------+

        ValueError occurred during unpack operation:

        too many values to unpack (expected 2)
        """
    )

    expect = RefCounts()
    try:
        unpack(TupleWrongLen, buf00)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect
