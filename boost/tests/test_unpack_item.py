# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Test reference count handling in boost unpack_item() function."""

from baseline import Baseline

from plum.exceptions import UnpackError
from plum.utilities import unpack
from plum.int import IntX

from .utils import RefCounts as RefCountsBase

uint8 = IntX("uint8", nbytes=1)

buf00 = b"\x00"
buf0001 = b"\x00\x01"
key_a = "a"
key_b = "b"


class RefCounts(RefCountsBase):

    """Python object reference count tracker."""

    def __init__(self):
        super().__init__(
            self,
            uint8,
            buf00=buf00,
            buf0001=buf0001,
            key_a=key_a,
            key_b=key_b,
        )


def test_plum():
    """Test unpacking with just plum type.

    branches: UNPACK_ITEM:PLUM

    """
    expect = RefCounts()
    retval = unpack(uint8, buf00)
    actual = RefCounts()

    assert retval == 0
    assert actual == expect


def test_dict():
    """Test unpacking with dict format.

    branches: UNPACK_ITEM:DICT

    """
    retval = key_a, key_b  # increment refs to keys (to compensate for keys returned)
    expect = RefCounts()
    retval = unpack({key_a: uint8, key_b: uint8}, buf0001)
    actual = RefCounts()

    assert retval == {key_a: 0, key_b: 1}
    assert actual == expect


def test_list():
    """Test unpacking with list format.

    branches: UNPACK_ITEM:LIST

    """
    expect = RefCounts()
    retval = unpack([uint8, uint8], buf0001)
    actual = RefCounts()

    assert retval == [0, 1]
    assert actual == expect


def test_tuple():
    """Test unpacking with tuple format.

    branches: UNPACK_ITEM:TUPLE

    """
    expect = RefCounts()
    retval = unpack((uint8, uint8), buf0001)
    actual = RefCounts()

    assert retval == (0, 1)
    assert actual == expect


def test_bad_fmt():
    """Test unpacking with bad format.

    branches: UNPACK_ITEM:BAD_FMT

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+------+
        | Offset | Value | Bytes | Type |
        +--------+-------+-------+------+
        |        |       |       | 0    |
        +--------+-------+-------+------+

        TypeError occurred during unpack operation:

        fmt must specify a Data type (or a dict, tuple, or list of them)
        """
    )

    expect = RefCounts()
    try:
        unpack(0, buf00)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect
