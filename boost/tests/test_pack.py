# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Test reference count handling in boost pack() function."""

from baseline import Baseline

from plum.exceptions import PackError
from plum.utilities import pack
from plum.int import IntX

from .utils import RefCounts as RefCountsBase

uint8 = IntX("uint8", nbytes=1)

key_a = "a"
fmt_dict = {key_a: uint8}
fmt_empty_dict = {}
fmt_list = [uint8]
fmt_tuple = tuple(fmt_list)
invalid_value = "hello world"


class RefCounts(RefCountsBase):

    """Python object reference count tracker."""

    def __init__(self):
        super().__init__(
            self,
            uint8,
            fmt_dict=fmt_dict,
            fmt_empty_dict=fmt_empty_dict,
            fmt_list=fmt_list,
            fmt_tuple=fmt_tuple,
            invalid_value=invalid_value,
            key_a=key_a,
        )


def test_one_pos_arg():
    """Test only one positional argument (fmt).

    branches: PACK:HAS_ARGS
    """
    expect = RefCounts()
    retval = pack(dict(a=0), fmt_dict)
    actual = RefCounts()

    assert retval == b"\x00"
    assert actual == expect


def test_two_pos_arg():
    """Test multiple positional arguments (fmt plus others).

    branches: PACK:HAS_ARGS
    """
    expect = RefCounts()
    retval = pack(uint8, 0)
    actual = RefCounts()

    assert retval == b"\x00"
    assert actual == expect


def test_no_pos_arg():
    """Test no positional arguments.

    branches: PACK:NO_ARGS
    """
    expect = RefCounts()
    retval = pack(dict(a=0), fmt=fmt_dict)
    actual = RefCounts()

    assert retval == b"\x00"
    assert actual == expect


no_fmt_message = Baseline(
    """
    pack() missing 1 required positional argument: 'fmt'
    """
)


def test_no_fmt_no_kw():
    """Test no format argument (with no keyword arg).

    branches: PACK:NO_FMT
    branches: PACK:ERROR
    """
    expect = RefCounts()
    try:
        pack()
    except TypeError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == no_fmt_message
    assert actual == expect


def test_no_fmt_with_kw():
    """Test no format argument (with keyword arg).

    branches: PACK:NO_FMT
    branches: PACK:ERROR

    """
    expect = RefCounts()
    try:
        pack(a=0)
    except TypeError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == no_fmt_message
    assert actual == expect


dup_fmt_message = Baseline(
    """
    pack() got multiple values for argument 'fmt'
    """
)


def test_dup_fmt_no_extra_kw():
    """Test both position and keyword format argument (with no other keyword argument).

    branches: PACK:HAS_DUP_FMT
    branches: PACK:ERROR

    """
    expect = RefCounts()
    try:
        pack(0, fmt=uint8)
    except TypeError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == dup_fmt_message
    assert actual == expect


def test_dup_fmt_with_extra_kw():
    """Test both position and keyword format argument (with other keyword argument).

    branches: PACK:HAS_DUP_FMT
    """
    expect = RefCounts()
    try:
        pack(fmt_dict, fmt=fmt_dict, a=0)
    except TypeError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == dup_fmt_message
    assert actual == expect


def test_has_kwds_fmt_kwd():
    """Test has keyword arguments with fmt keyword argument.

    branches: PACK:HAS_KWDS
    branches: PACK:HAS_KWDS:FMT_KWD

    """
    expect = RefCounts()
    retval = pack(fmt=fmt_dict, a=0)
    actual = RefCounts()

    assert retval == b"\x00"
    assert actual == expect


def test_has_kwds_no_fmt_kwd():
    """Test has keyword arguments with no fmt keyword argument.

    branches: PACK:HAS_KWDS
    branches: PACK:HAS_KWDS:NO_FMT_KWD

    """
    expect = RefCounts()
    retval = pack(fmt_dict, a=0)
    actual = RefCounts()

    assert retval == b"\x00"
    assert actual == expect


def test_fmt_is_plum():
    """Test plum type passed as format.

    branches: PACK:FMT_IS_PLUM

    """
    expect = RefCounts()
    retval = pack(uint8, 0)
    actual = RefCounts()

    assert retval == b"\x00"
    assert actual == expect


def test_fmt_is_plum_extra_values():
    """Test plum type passed as format.

    branches: PACK:FMT_IS_PLUM
    branches: PACK:FMT_IS_PLUM:EXTRA_VALUES
    branches: PACK:ERROR:RETRY

    """
    exp_message = Baseline(
        """


        +--------+-------+-------+--------------+
        | Offset | Value | Bytes | Format       |
        +--------+-------+-------+--------------+
        | 0      | 0     | 00    | uint8        |
        |        | 1     |       | (unexpected) |
        +--------+-------+-------+--------------+

        TypeError occurred during pack operation:

        2 values given, expected 1
        """
    )

    expect = RefCounts()
    try:
        pack(uint8, 0, 1)
    except PackError as exc:
        message = str(exc)
    actual = RefCounts()

    assert message == exp_message
    assert actual == expect


def test_fmt_is_tuple():
    """Test format is a tuple.

    branches: PACK:FMT_IS_SEQ

    """
    expect = RefCounts()
    retval = pack(fmt_tuple, 0)
    actual = RefCounts()

    assert retval == b"\x00"
    assert actual == expect


def test_fmt_is_tuple_with_keywords():
    """Test format is a tuple, but passed keyword arguments.

    branches: PACK:FMT_IS_SEQ
    branches: PACK:FMT_IS_SEQ:WITH_KWDS
    branches: PACK:ERROR:RETRY

    """
    exp_message = Baseline(
        """


        +--------+--------+-------+-------+--------------+
        | Offset | Access | Value | Bytes | Format       |
        +--------+--------+-------+-------+--------------+
        | 0      | [0]    | 0     | 00    | uint8        |
        +--------+--------+-------+-------+--------------+
        |        | ['a']  | 1     |       | (unexpected) |
        +--------+--------+-------+-------+--------------+

        TypeError occurred during pack operation:

        unexpected keyword argument value: 'a'
        """
    )

    expect = RefCounts()
    try:
        pack(fmt_tuple, 0, a=1)
    except PackError as exc:
        message = str(exc)
    actual = RefCounts()

    assert message == exp_message
    assert actual == expect


def test_fmt_is_list():
    """Test format is a list.

    branches: PACK:FMT_IS_SEQ

    """
    expect = RefCounts()
    retval = pack(fmt_list, 0)
    actual = RefCounts()

    assert retval == b"\x00"
    assert actual == expect


def test_fmt_is_list_with_keywords():
    """Test format is a list, but passed keyword arguments.

    branches: PACK:FMT_IS_SEQ
    branches: PACK:FMT_IS_SEQ:WITH_KWDS
    branches: PACK:ERROR:RETRY

    """
    exp_message = Baseline(
        """


        +--------+--------+-------+-------+--------------+
        | Offset | Access | Value | Bytes | Format       |
        +--------+--------+-------+-------+--------------+
        | 0      | [0]    | 0     | 00    | uint8        |
        +--------+--------+-------+-------+--------------+
        |        | ['a']  | 1     |       | (unexpected) |
        +--------+--------+-------+-------+--------------+

        TypeError occurred during pack operation:

        unexpected keyword argument value: 'a'
        """
    )

    expect = RefCounts()
    try:
        pack(fmt_list, 0, a=1)
    except PackError as exc:
        message = str(exc)
    actual = RefCounts()

    assert message == exp_message
    assert actual == expect


def test_fmt_is_dict():
    """Test format is a dict.

    branches: PACK:FMT_IS_DICT:PACKING_KWDS

    """
    expect = RefCounts()
    retval = pack(fmt_dict, a=0)
    actual = RefCounts()

    assert retval == b"\x00"
    assert actual == expect


def test_fmt_is_dict_with_position_args():
    """Test format is a dict, but passed positional arguments.

    branches: PACK:FMT_IS_DICT
    branches: PACK:FMT_IS_DICT:WITH_ARGS
    branches: PACK:ERROR:RETRY

    """
    exp_message = Baseline(
        """


        +--------+--------+-------+-------+--------------+
        | Offset | Access | Value | Bytes | Format       |
        +--------+--------+-------+-------+--------------+
        | 0      | ['a']  | 0     | 00    | uint8        |
        +--------+--------+-------+-------+--------------+
        |        |        | 0     |       | (unexpected) |
        +--------+--------+-------+-------+--------------+

        TypeError occurred during pack operation:

        got 1 unexpected value
        """
    )

    expect = RefCounts()
    try:
        pack(fmt_dict, 0, a=0)
    except PackError as exc:
        message = str(exc)
    actual = RefCounts()

    assert message == exp_message
    assert actual == expect


def test_fmt_is_dict_no_values():
    """Test format is a dict, but passed no values.

    branches: PACK:FMT_IS_DICT
    branches: PACK:FMT_IS_DICT:NO_VALUES
    branches: PACK:ERROR:RETRY

    """
    exp_message = Baseline(
        """


        +--------+--------+-----------+-------+-------+
        | Offset | Access | Value     | Bytes | Format|
        +--------+--------+-----------+-------+-------+
        |        | ['a']  | (missing) |       | uint8 |
        +--------+--------+-----------+-------+-------+

        TypeError occurred during pack operation:

        missing value: 'a'
        """
    )

    expect = RefCounts()
    try:
        pack(fmt_dict)
    except PackError as exc:
        message = str(exc)
    actual = RefCounts()

    assert message == exp_message
    assert actual == expect


def test_fmt_is_empty_dict():
    """Test format is an empty dict.

    branches: PACK:FMT_IS_DICT:BUT_EMPTY
    branches: PACK:NO_VALUES_PACKED

    """
    expect = RefCounts()
    retval = pack(fmt_empty_dict)
    actual = RefCounts()

    assert retval == b""
    assert actual == expect


def test_fmt_is_invalid_with_value():
    """Test format is invalid.

    branches: PACK:INVALID_FMT
    branches: PACK:ERROR:RETRY

    """
    exp_message = Baseline(
        """


        +--------+-------+-------+-------------+
        | Offset | Value | Bytes | Format      |
        +--------+-------+-------+-------------+
        |        | 1     |       | 0 (invalid) |
        +--------+-------+-------+-------------+

        TypeError occurred during pack operation:

        bad format, expected tuple, list, dict, or DataMeta, got 0
        """
    )

    expect = RefCounts()
    try:
        pack(0, 1)
    except PackError as exc:
        message = str(exc)
    actual = RefCounts()

    assert message == exp_message
    assert actual == expect


def test_fmt_is_invalid_without_value():
    """Test format is invalid and without a value.

    branches: PACK:INVALID_FMT
    branches: PACK:ERROR:RETRY

    """
    exp_message = Baseline(
        """


        +--------+-------+-------+-------------+
        | Offset | Value | Bytes | Format      |
        +--------+-------+-------+-------------+
        |        | {}    |       | 0 (invalid) |
        +--------+-------+-------+-------------+

        TypeError occurred during pack operation:

        bad format, expected tuple, list, dict, or DataMeta, got 0
        """
    )

    expect = RefCounts()
    try:
        pack(0)
    except PackError as exc:
        message = str(exc)
    actual = RefCounts()

    assert message == exp_message
    assert actual == expect


def test_failed_pack():
    """Test format is ok, but invalid value.

    branches: PACK:FAILED_PACK
    branches: PACK:ERROR:RETRY

    """
    exp_message = Baseline(
        """


        +--------+---------------+-------+-------+
        | Offset | Value         | Bytes | Format|
        +--------+---------------+-------+-------+
        |        | 'hello world' |       | uint8 |
        +--------+---------------+-------+-------+

        TypeError occurred during pack operation:

        value type <class 'str'> not int-like (no to_bytes() method)
        """
    )

    expect = RefCounts()
    try:
        pack(uint8, invalid_value)
    except PackError as exc:
        message = str(exc)
    actual = RefCounts()

    assert message == exp_message
    assert actual == expect
