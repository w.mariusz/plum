# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Test reference count handling in boost pack() function."""

from baseline import Baseline

from plum import pack
from plum.exceptions import PackError
from plum.int import IntX
from plum.enum import Enum

from ..utils import RefCounts as RefCountsBase

uint8 = IntX("uint8", nbytes=1)

bad_value = "junk"


class Pet(Enum, nbytes=1, strict=False):

    """Sample tolerant enumeration (not strict)."""

    CAT = 0
    DOG = 1


class StrictEnum(Enum, nbytes=1, strict=True):

    """Sample strict enumeration."""

    A = 1


class RefCounts(RefCountsBase):

    """Python object reference count tracker."""

    def __init__(self):
        super().__init__(
            self,
            uint8,
            bad_value=bad_value,
            pet_cls=Pet,
            pet_cat=Pet.CAT,
            pet_dog=Pet.DOG,
            strict_cls=StrictEnum,
            strict_a=StrictEnum.A,
        )


def test_bad_value():
    """Test error packing.

    branches: INT_PACK:ERROR

    """
    expect_message = Baseline(
        """


        +--------+--------+-------+-------+
        | Offset | Value  | Bytes | Format|
        +--------+--------+-------+-------+
        |        | 'junk' |       | uint8 |
        +--------+--------+-------+-------+

        TypeError occurred during pack operation:

        value type <class 'str'> not int-like (no to_bytes() method)
        """
    )

    expect = RefCounts()
    try:
        pack(bad_value, uint8)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_bad_member():
    """Test error packing because value not part of strict enumeration.

    branches: INT_PACK:VERIFY_STRICT:NOT_MEMBER

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+------------+
        | Offset | Value | Bytes | Format     |
        +--------+-------+-------+------------+
        |        | 0     |       | StrictEnum |
        +--------+-------+-------+------------+

        ValueError occurred during pack operation:

        0 is not a valid StrictEnum
        """
    )

    expect = RefCounts()
    try:
        pack(0, StrictEnum)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_valid_enumeration_value():
    """Test pack successful with strict enum value.

    branches: INT_PACK:VERIFY_STRICT

    """
    expect = RefCounts()
    retval = pack(1, StrictEnum)
    actual = RefCounts()

    assert actual == expect
    assert retval == b"\x01"


def test_valid_enumeration_member():
    """Test pack successful with strict enum member.

    branches: INT_PACK

    """
    expect = RefCounts()
    retval = pack(StrictEnum.A, StrictEnum)
    actual = RefCounts()

    assert actual == expect
    assert retval == b"\x01"
