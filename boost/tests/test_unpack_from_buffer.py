# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Test reference count handling in boost unpack_from_buffer() function."""

from io import BytesIO

from baseline import Baseline

from plum.data import Plum, PlumMeta
from plum.exceptions import UnpackError
from plum.int import IntX
from plum.utilities import getbytes, unpack, unpack_from

from .utils import RefCounts as RefCountsBase

uint8 = IntX("uint8", nbytes=1)


class CustomError(Plum, metaclass=PlumMeta):  # pylint: disable=abstract-method

    """Always raises ValueError when unpacked."""

    @classmethod
    def __unpack__(cls, buffer, offset, parent, dump):
        if dump:
            dump.value = "<invalid>"
            getbytes(buffer, offset, dump, 1)
        raise ValueError("invalid byte")


class BinFileBadTell:

    """Binary file without proper methods."""


class BinFileBadSeek(BytesIO):

    """Binary file with seek method that fails."""

    def seek(self, index):
        """Raise error."""
        raise ValueError("Seek method error.")


class BinFileBadRead(BytesIO):

    """Binary file with Read method that fails."""

    def __init__(self, buffer):
        self.num_reads = 0
        super().__init__(buffer)

    def read(self, size=-1):
        """Read bytes from buffer."""
        self.num_reads += 1
        if (self.num_reads & 1) == 0:
            raise ValueError("read method error")
        return super().read(size)


binfile_bad_read = BinFileBadRead(b"\x00")

buf00 = b"\x00"
binfile_bad_tell = BinFileBadTell()
binfile_bad_seek = BinFileBadSeek()

binfile_00 = BytesIO(b"\x00")
binfile_0001 = BytesIO(b"\x00\x01")
bytesbuf_00 = b"\x00"
bytesbuf_0001 = b"\x00\x01"

invalid_offset = "bad"


class RefCounts(RefCountsBase):

    """Python object reference count tracker."""

    def __init__(self):
        super().__init__(
            self,
            uint8,
            buf00=buf00,
            binfile_bad_read=binfile_bad_read,
            binfile_bad_read_cls=BinFileBadRead,
            binfile_bad_seek=binfile_bad_seek,
            binfile_bad_seek_cls=BinFileBadSeek,
            binfile_bad_tell=binfile_bad_tell,
            binfile_bad_tell_cls=BinFileBadTell,
            binfile_00=binfile_00,
            binfile_0001=binfile_0001,
            bytesbuf_00=bytesbuf_00,
            bytesbuf_0001=bytesbuf_0001,
            invalid_offset=invalid_offset,
        )


def test_binfile_happy_path_no_offset():
    """Test with binary file buffer with no offset.

    branches: UNPACK_FB:BINFILE:TELL_OFF:SUCCEED

    """
    try:
        expect = RefCounts()
        retval = unpack(uint8, binfile_00)
        actual = RefCounts()

        assert actual == expect
        assert retval == 0
    finally:
        binfile_00.seek(0)


def test_binfile_tell_fail():
    """Test with binary file buffer but tell() call fails.

    branches: UNPACK_FB:BINFILE:TELL_OFF:FAIL

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+-------+
        | Offset | Value | Bytes | Format|
        +--------+-------+-------+-------+
        |        |       |       | uint8 |
        +--------+-------+-------+-------+

        AttributeError occurred during unpack operation:

        'BinFileBadTell' object has no attribute 'read'
        """
    )

    expect = RefCounts()
    try:
        unpack(uint8, binfile_bad_tell)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_binfile_seek_fail():
    """Test with binary file buffer but seek() call fails.

    branches: UNPACK_FB:BINFILE:OFF_ARG:SEEK_FAIL

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+------+
        | Offset | Value | Bytes | Type |
        +--------+-------+-------+------+
        |        |       |       |      |
        +--------+-------+-------+------+

        ValueError occurred during unpack operation:

        Seek method error.
        """
    )

    expect = RefCounts()
    try:
        unpack_from(uint8, binfile_bad_seek, 0)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_binfile_happy_path_with_offset():
    """Test with binary file buffer with offset.

    branches: UNPACK_FB:BINFILE:OFF_ARG:SEEK_OK

    """
    try:
        expect = RefCounts()
        retval = unpack_from(uint8, binfile_00, 0)
        actual = RefCounts()

        assert actual == expect
        assert retval == 0
    finally:
        binfile_00.seek(0)


def test_binfile_bad_offset():
    """Test with binary file buffer but offset is bad.

    branches: UNPACK_FB:BINFILE:OFF_ARG:BAD
    branches: UNPACK_FROM:ERROR

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+------+
        | Offset | Value | Bytes | Type |
        +--------+-------+-------+------+
        |        |       |       |      |
        +--------+-------+-------+------+

        TypeError occurred during unpack operation:

        'str' object cannot be interpreted as an integer
        """
    )

    expect = RefCounts()
    try:
        unpack_from(uint8, binfile_00, invalid_offset)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_bytes_default_offset():
    """Test with bytes buffer with default offset.

    branches: UNPACK_FB:BYTEBUF:DEFAULT_OFFSET

    """
    expect = RefCounts()
    retval = unpack_from(uint8, bytesbuf_00)
    actual = RefCounts()

    assert actual == expect
    assert retval == 0


def test_bytes_offset_ok():
    """Test with bytes buffer with valid offset.

    branches: UNPACK_FB:BYTEBUF:OFFSET_ARG:OK
    branches: UNPACK_FB:RELEASE_BUF

    """
    expect = RefCounts()
    retval = unpack_from(uint8, bytesbuf_00, 0)
    actual = RefCounts()

    assert actual == expect
    assert retval == 0


def test_bytes_offset_bad():
    """Test with bytes buffer and bad offset.

    branches: UNPACK_FB:BYTEBUF:OFFSET_ARG:BAD

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+------+
        | Offset | Value | Bytes | Type |
        +--------+-------+-------+------+
        |        |       |       |      |
        +--------+-------+-------+------+

        ValueError occurred during unpack operation:

        invalid literal for int() with base 10: 'bad'
        """
    )

    expect = RefCounts()
    try:
        unpack_from(uint8, bytesbuf_00, invalid_offset)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_check_excess_bytes():
    """Test for excess in bytes buffer.

    branches: UNPACK_FB:CHECK_EXCESS:BYTEBUF

    """
    expect = RefCounts()
    retval = unpack(uint8, bytesbuf_00)
    actual = RefCounts()

    assert actual == expect
    assert retval == 0


def test_check_excess_binfile_read_ok():
    """Test for excess in binary file buffer.

    branches: UNPACK_FB:CHECK_EXCESS:BINFILE:READ_OK

    """
    try:
        expect = RefCounts()
        retval = unpack(uint8, binfile_00)
        actual = RefCounts()

        assert actual == expect
        assert retval == 0
    finally:
        binfile_00.seek(0)


def test_check_excess_binfile_read_fail():
    """Test for excess in binary file buffer but excess read fails.

    branches: UNPACK_FB:CHECK_EXCESS:BINFILE:READ_FAIL

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+-------+
        | Offset | Value | Bytes | Format|
        +--------+-------+-------+-------+
        | 0      | 0     | 00    | uint8 |
        +--------+-------+-------+-------+

        ValueError occurred during unpack operation:

        read method error
        """
    )

    expect = RefCounts()
    try:
        unpack(uint8, binfile_bad_read)
    except UnpackError as exc:
        exc_message = str(exc)
    finally:
        binfile_bad_read.num_reads = 0
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_check_bytes_has_excess():
    """Test bytes buffer has excess.

    branches: UNPACK_FB:CHECK_EXCESS:ERROR
    branches: UNPACK_FB:ERROR

    """
    expect_message = Baseline(
        """


        +--------+----------------+-------+-------+
        | Offset | Value          | Bytes | Format|
        +--------+----------------+-------+-------+
        | 0      | 0              | 00    | uint8 |
        +--------+----------------+-------+-------+
        | 1      | <excess bytes> | 01    |       |
        +--------+----------------+-------+-------+

        ExcessMemoryError occurred during unpack operation:

        1 unconsumed bytes
        """
    )

    expect = RefCounts()
    try:
        unpack(uint8, bytesbuf_0001)
    except UnpackError as exc:
        exc_message = str(exc)
    finally:
        binfile_bad_read.num_reads = 0
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_check_binfile_has_excess():
    """Test binary file buffer has excess.

    branches: UNPACK_FB:CHECK_EXCESS:ERROR
    branches: UNPACK_FB:ERROR:SEEK

    """
    expect_message = Baseline(
        """


        +--------+----------------+-------+-------+
        | Offset | Value          | Bytes | Format|
        +--------+----------------+-------+-------+
        | 0      | 0              | 00    | uint8 |
        +--------+----------------+-------+-------+
        | 1      | <excess bytes> | 01    |       |
        +--------+----------------+-------+-------+

        ExcessMemoryError occurred during unpack operation:

        1 unconsumed bytes
        """
    )

    expect = RefCounts()
    try:
        unpack(uint8, binfile_0001)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_kw_error():
    """Test bad keyword argument.

    branches: UNPACK_FROM:KW_FAIL

    """
    expect = RefCounts()
    try:
        unpack_from(uint8, butter=binfile_00)
    except TypeError:
        pass
    actual = RefCounts()

    assert actual == expect
