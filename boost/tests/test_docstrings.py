# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Verify boost utility docstrings match Python based utilities."""

from plum.data import Plum, pack_doc, packmethod_doc, unpack_doc, unpack_from_doc
from plum.utilities import pack, unpack, unpack_from


def test_pack():
    """Test boost pack() docstring."""
    assert pack.__doc__ == pack_doc


def _test_packmethod():
    """Test boost Data.pack() docstring."""
    # FUTURE re-enable test when method doesn't use functools.partial
    assert Plum.pack.__doc__ == packmethod_doc


def test_unpack():
    """Test boost unpack() docstring."""
    assert unpack.__doc__ == unpack_doc
