# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Verify boost branch trace markers are covered by test cases."""

import os
import re

from baseline import Baseline

THIS_DIR = os.path.abspath(os.path.dirname(__file__))
BOOST_DIR = os.path.dirname(THIS_DIR)

TRACE_RE = re.compile(r'tracef\("\s*([\w:]+)"\)')
COVER_RE = re.compile(r"^\s+branches\:\s+(.*)", re.MULTILINE)


def register(branches, branch):
    """Register branch (and all parent branches) into branches set.

    :param set branches: branch names
    :param str branch: branch name

    """
    segments = []
    for segment in branch.split(":"):
        segments.append(segment)
        branches.add(":".join(segments))


def inventory_code_branches():
    """Produce set of branch names found in "C" source files."""
    branches = set()
    for path, _dirs, files in os.walk(os.path.join(BOOST_DIR, "src", "extmods")):
        for filename in files:
            if os.path.splitext(filename)[1] in {".c", ".h"}:
                with open(os.path.join(path, filename)) as source:
                    content = source.read()
                for branch in TRACE_RE.findall(content):
                    register(branches, branch)
    return branches


def inventory_test_branches():
    """Produce set of branch names covered in regression tests."""
    branches = set()
    for path, _dirs, files in os.walk(os.path.join(THIS_DIR)):
        for filename in files:
            if filename.endswith(".py"):
                with open(os.path.join(path, filename)) as source:
                    content = source.read()
                for line_content in COVER_RE.findall(content):
                    register(branches, line_content.strip())
    return branches


code_branches = inventory_code_branches()
test_branches = inventory_test_branches()

# FUTURE - think about covering these branches (or putting comments on ones that are OK)
reviewed_branches_summary = """

    # Pretty safe, logic based on Python implementation of @classmethod
    DUNDERPACKMETHOD_CALL
    DUNDERPACKMETHOD_CALL:KEYWORDS_FAIL
    DUNDERUNPACKMETHOD_CALL
    DUNDERUNPACKMETHOD_CALL:KEYWORDS_FAIL
    
    # FUTURE - no boosted type that is variable in size
    GETBYTES:BINFILE:READ_ALL

    # FUTURE - this is being re-done when ipack() comes, test then
    PACKMETHOD_CALL
    PACKMETHOD_CALL
    PACKMETHOD_GET
    PACKMETHOD_GET:CLASS
    PACKMETHOD_GET:INSTANCE
    PACKMETHOD_INIT
    
    # FUTURE - no types yet where potentially fast but can't pack fast
    PACK_VAL:FASTDATA_AND_FIXED:SLOW_PACK
    PACK_VAL:FASTDATA_AND_FIXED:SLOW_PACK:FAIL
    PACK_VAL:FASTDATA_AND_FIXED:SLOW_PACK:SUCCESS

    # FUTURE - no types yet that are fast but variable in size
    PACK_VAL:SLOW_OR_VAR:FAST_PACK
    """

reviewed_branches = set()
for line in reviewed_branches_summary.split("\n"):
    line = line.strip()
    if line and not line.startswith("#"):
        reviewed_branches.add(line)


def test_missing_coverage():
    """Check branches in code are covered by test cases."""
    missing = code_branches - test_branches - reviewed_branches
    print()
    for extra in sorted(missing):
        print(extra)
    assert not missing, "code branches missing test cases"


def test_invalid_names():
    """Check test cases have valid branch names."""
    extras = test_branches - code_branches
    assert not extras, "invalid names found in test cases"


def test_branch_inventory():
    """Check names found against reviewable list."""
    branches_found = Baseline(
        """
    DUNDERPACKMETHOD_CALL
    DUNDERPACKMETHOD_CALL:KEYWORDS_FAIL
    DUNDERUNPACKMETHOD_CALL
    DUNDERUNPACKMETHOD_CALL:KEYWORDS_FAIL
    GETBYTES
    GETBYTES:BINFILE
    GETBYTES:BINFILE:READ_ALL
    GETBYTES:BINFILE:READ_ERROR
    GETBYTES:BINFILE:READ_NBYTES
    GETBYTES:BINFILE:READ_OK
    GETBYTES:BYTEBUF
    GETBYTES:BYTEBUF:NBYTES_DIFFERS
    GETBYTES:ERROR
    GETBYTES:GOT_BYTES
    GETBYTES:GOT_BYTES:OK
    GETBYTES:GOT_BYTES:SHORTAGE
    INT_PACK
    INT_PACK:ERROR
    INT_PACK:VERIFY_STRICT
    INT_PACK:VERIFY_STRICT:NOT_MEMBER
    INT_UNPACK
    INT_UNPACK:GET_INT
    INT_UNPACK:INT_BAD
    INT_UNPACK:INT_OK
    INT_UNPACK:INT_OK:CONVERT
    INT_UNPACK:INT_OK:CONVERT:FAIL
    INT_UNPACK:INT_OK:CONVERT:FAIL:ACCEPTABLE
    INT_UNPACK:INT_OK:CONVERT:FAIL:STRICT
    INT_UNPACK:INT_OK:CONVERT:OK
    INT_UNPACK:INT_OK:KEEP_AS_INT
    PACK
    PACK:ERROR
    PACK:ERROR:RETRY
    PACK:FAILED_PACK
    PACK:FMT_IS_DICT
    PACK:FMT_IS_DICT:BUT_EMPTY
    PACK:FMT_IS_DICT:NO_VALUES
    PACK:FMT_IS_DICT:PACKING_KWDS
    PACK:FMT_IS_DICT:WITH_ARGS
    PACK:FMT_IS_PLUM
    PACK:FMT_IS_PLUM:EXTRA_VALUES
    PACK:FMT_IS_SEQ
    PACK:FMT_IS_SEQ:WITH_KWDS
    PACK:HAS_ARGS
    PACK:HAS_DUP_FMT
    PACK:HAS_KWDS
    PACK:HAS_KWDS:FMT_KWD
    PACK:HAS_KWDS:NO_FMT_KWD
    PACK:INVALID_FMT
    PACK:NO_ARGS
    PACK:NO_FMT
    PACK:NO_VALUES_PACKED
    PACKMETHOD_CALL
    PACKMETHOD_GET
    PACKMETHOD_GET:CLASS
    PACKMETHOD_GET:INSTANCE
    PACKMETHOD_INIT
    PACK_VAL
    PACK_VAL:FASTDATA_AND_FIXED
    PACK_VAL:FASTDATA_AND_FIXED:BUF_CREATE
    PACK_VAL:FASTDATA_AND_FIXED:BUF_EXIST
    PACK_VAL:FASTDATA_AND_FIXED:BUF_EXIST:RESIZING
    PACK_VAL:FASTDATA_AND_FIXED:FAST_PACK
    PACK_VAL:FASTDATA_AND_FIXED:SLOW_PACK
    PACK_VAL:FASTDATA_AND_FIXED:SLOW_PACK:FAIL
    PACK_VAL:FASTDATA_AND_FIXED:SLOW_PACK:SUCCESS
    PACK_VAL:SLOW_OR_VAR
    PACK_VAL:SLOW_OR_VAR:BUF_CREATE
    PACK_VAL:SLOW_OR_VAR:FAST_PACK
    PACK_VAL:SLOW_OR_VAR:SLOW_PACK
    PACK_VAL:SLOW_OR_VAR:SLOW_PACK:FAIL
    PACK_VAL:SLOW_OR_VAR:SLOW_PACK:SUCCESS
    PACK_VAL:VAL_IS_VIEW
    PACK_VAL:VAL_IS_VIEW:GET_FAIL
    PACK_VAL:VAL_IS_VIEW:GET_SUCCESS
    PACK_VWF
    PACK_VWF:FMT_INVALID
    PACK_VWF:FMT_IS_DICT
    PACK_VWF:FMT_IS_DICT:VAL_IS_DICT
    PACK_VWF:FMT_IS_DICT:VAL_IS_DICT:MISSING_KEY
    PACK_VWF:FMT_IS_DICT:VAL_IS_DICT:UNEQUAL_LEN
    PACK_VWF:FMT_IS_DICT:VAL_NOT_DICT
    PACK_VWF:FMT_IS_PLUM
    PACK_VWF:FMT_IS_SEQ
    PACK_VWF:FMT_IS_SEQ:VAL_IS_NOT_SEQ
    PACK_VWF:FMT_IS_SEQ:VAL_IS_SEQ
    PACK_VWF:FMT_IS_SEQ:VAL_IS_SEQ:UNEQUAL_LEN
    STRUCT_PACK
    STRUCT_PACK:ITEM_FAIL
    STRUCT_PACK:TYPE_BAD
    STRUCT_PACK:TYPE_DICT
    STRUCT_PACK:TYPE_DICT:BAD_KEYWORDS
    STRUCT_PACK:TYPE_SEQ
    STRUCT_PACK:TYPE_SEQ:RIGHT_SIZE
    STRUCT_PACK:TYPE_SEQ:WRONG_SIZE
    STRUCT_UNPACK
    STRUCT_UNPACK:ITEM_FAIL
    UNPACK
    UNPACK:ERROR
    UNPACK:POS_FAIL
    UNPACK_DICT
    UNPACK_DICT:WHILE
    UNPACK_DICT:WHILE:ITEM_BAD
    UNPACK_DICT:WHILE:ITEM_OK
    UNPACK_FB
    UNPACK_FB:BINFILE
    UNPACK_FB:BINFILE:OFF_ARG
    UNPACK_FB:BINFILE:OFF_ARG:BAD
    UNPACK_FB:BINFILE:OFF_ARG:SEEK_FAIL
    UNPACK_FB:BINFILE:OFF_ARG:SEEK_OK
    UNPACK_FB:BINFILE:TELL_OFF
    UNPACK_FB:BINFILE:TELL_OFF:FAIL
    UNPACK_FB:BINFILE:TELL_OFF:SUCCEED
    UNPACK_FB:BYTEBUF
    UNPACK_FB:BYTEBUF:DEFAULT_OFFSET
    UNPACK_FB:BYTEBUF:OFFSET_ARG
    UNPACK_FB:BYTEBUF:OFFSET_ARG:BAD
    UNPACK_FB:BYTEBUF:OFFSET_ARG:OK
    UNPACK_FB:CHECK_EXCESS
    UNPACK_FB:CHECK_EXCESS:BINFILE
    UNPACK_FB:CHECK_EXCESS:BINFILE:READ_FAIL
    UNPACK_FB:CHECK_EXCESS:BINFILE:READ_OK
    UNPACK_FB:CHECK_EXCESS:BYTEBUF
    UNPACK_FB:CHECK_EXCESS:ERROR
    UNPACK_FB:ERROR
    UNPACK_FB:ERROR:SEEK
    UNPACK_FB:RELEASE_BUF
    UNPACK_FROM
    UNPACK_FROM:ERROR
    UNPACK_FROM:KW_FAIL
    UNPACK_ITEM
    UNPACK_ITEM:BAD_FMT
    UNPACK_ITEM:DICT
    UNPACK_ITEM:LIST
    UNPACK_ITEM:PLUM
    UNPACK_ITEM:TUPLE
    UNPACK_LIST
    UNPACK_LIST:FOR
    UNPACK_LIST:FOR:ITEM_BAD
    UNPACK_LIST:FOR:ITEM_OK
    UNPACK_PLUM
    UNPACK_PLUM:FAST
    UNPACK_PLUM:SLOW
    UNPACK_PLUM:SLOW:ERROR
    UNPACK_PLUM:SLOW:SUCCEED
    UNPACK_PLUM:SLOW:SUCCEED:TUPLE_OK
    UNPACK_PLUM:SLOW:SUCCEED:TUPLE_OK:OFFSET_BAD
    UNPACK_PLUM:SLOW:SUCCEED:TUPLE_OK:OFFSET_OK
    UNPACK_PLUM:SLOW:TUPLE_BAD
    UNPACK_TUPLE
    UNPACK_TUPLE:FOR
    UNPACK_TUPLE:FOR:ITEM_BAD
    UNPACK_TUPLE:FOR:ITEM_OK
    """
    )

    sorted_names = "\n".join(sorted(code_branches))

    assert sorted_names == branches_found
