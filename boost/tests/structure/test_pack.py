# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Test reference count handling in structure boost pack() function."""

from baseline import Baseline

from plum.exceptions import PackError
from plum.utilities import pack
from plum.int import IntX
from plum.structure import Structure, member

from ..utils import RefCounts as RefCountsBase

uint8 = IntX("uint8", nbytes=1)


class Sample(Structure):

    """Sample structure (embedded in another structure)."""

    inner_a: int = member(fmt=uint8)
    inner_b: int = member(fmt=uint8, default=1)


sample = Sample(inner_a=0, inner_b=1)

sample_bad_item = (0, "junk")
sample_dict = {"inner_a": 0, "inner_b": 1}
sample_extra_dict = {"inner_a": 0, "inner_b": 1, "inner_c": 2}
sample_extra_list = [0, 1, 2]
sample_extra_tuple = (0, 1, 2)
sample_list = [0, 1]
sample_tuple = (0, 1)
sample_short_dict = {"inner_a": 0}
sample_short_list = [0]
sample_short_tuple = (0,)


class RefCounts(RefCountsBase):

    """Python object reference count tracker."""

    def __init__(self):
        super().__init__(
            self,
            uint8,
            sample=sample,
            sample_bad_item=sample_bad_item,
            sample_cls=Sample,
            sample_dict=sample_dict,
            sample_extra_list=sample_extra_list,
            sample_extra_tuple=sample_extra_tuple,
            sample_list=sample_list,
            sample_tuple=sample_tuple,
            sample_short_dict=sample_short_dict,
            sample_short_list=sample_short_list,
            sample_short_tuple=sample_short_tuple,
        )


def test_right_size_list():
    """Test value (as a list) same size and packs successfully.

    branches: STRUCT_PACK:TYPE_SEQ
    branches: STRUCT_PACK:TYPE_SEQ:RIGHT_SIZE

    """
    expect = RefCounts()
    retval = pack(sample_list, Sample)
    actual = RefCounts()

    assert actual == expect
    assert retval == b"\x00\x01"


def test_right_size_tuple():
    """Test value (as a tuple) same size and packs successfully.

    branches: STRUCT_PACK:TYPE_SEQ

    """
    expect = RefCounts()
    retval = pack(sample_tuple, Sample)
    actual = RefCounts()

    assert actual == expect
    assert retval == b"\x00\x01"


def test_right_size_same_cls():
    """Test value (as class instance) same size and packs successfully.

    branches: STRUCT_PACK:TYPE_SEQ

    """
    expect = RefCounts()
    retval = pack(sample, Sample)
    actual = RefCounts()

    assert actual == expect
    assert retval == b"\x00\x01"


def test_right_members_dict():
    """Test value (as a dict) packs successfully.

    branches: STRUCT_PACK:TYPE_DICT

    """
    expect = RefCounts()
    retval = pack(sample_dict, Sample)
    actual = RefCounts()

    assert actual == expect
    assert retval == b"\x00\x01"


def test_missing_member_list():
    """Test value (as a list) missing member and fails.

    branches: STRUCT_PACK:TYPE_SEQ:WRONG_SIZE

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+--------+
        | Offset | Value | Bytes | Format |
        +--------+-------+-------+--------+
        |        |       |       | Sample |
        +--------+-------+-------+--------+

        ValueError occurred during pack operation:

        invalid value, 'Sample' pack expects an iterable of length 2, got an iterable of length 1
        """
    )

    expect = RefCounts()
    try:
        pack(sample_short_list, Sample)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_missing_member_tuple():
    """Test value (as a tuple) missing member and fails.

    branches: STRUCT_PACK:TYPE_SEQ:WRONG_SIZE

    """
    expect_message = Baseline(
        """


            +--------+-------+-------+--------+
            | Offset | Value | Bytes | Format |
            +--------+-------+-------+--------+
            |        |       |       | Sample |
            +--------+-------+-------+--------+

            ValueError occurred during pack operation:

            invalid value, 'Sample' pack expects an iterable of length 2, got an iterable of length 1
            """
    )

    expect = RefCounts()
    try:
        pack(sample_short_tuple, Sample)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_missing_member_dict():
    """Test value (as a dict) missing member and packs successfully.

    branches: STRUCT_PACK:TYPE_DICT

    """
    expect = RefCounts()
    retval = pack(sample_short_dict, Sample)
    actual = RefCounts()

    assert actual == expect
    assert retval == b"\x00\x01"


def test_extra_list_item():
    """Test list value with extra member (init fails).

    branches: STRUCT_PACK:TYPE_SEQ:WRONG_SIZE

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+--------+
        | Offset | Value | Bytes | Format |
        +--------+-------+-------+--------+
        |        |       |       | Sample |
        +--------+-------+-------+--------+

        ValueError occurred during pack operation:

        invalid value, 'Sample' pack expects an iterable of length 2, got an iterable of length 3
        """
    )

    expect = RefCounts()
    try:
        pack(sample_extra_list, Sample)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_extra_tuple_item():
    """Test tuple value with extra member (init fails).

    branches: STRUCT_PACK:TYPE_SEQ:WRONG_SIZE

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+--------+
        | Offset | Value | Bytes | Format |
        +--------+-------+-------+--------+
        |        |       |       | Sample |
        +--------+-------+-------+--------+

        ValueError occurred during pack operation:

        invalid value, 'Sample' pack expects an iterable of length 2, got an iterable of length 3
        """
    )

    expect = RefCounts()
    try:
        pack(sample_extra_tuple, Sample)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_extra_dict_item():
    """Test dict value with extra member (init fails).

    branches: STRUCT_PACK:TYPE_DICT:BAD_KEYWORDS

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+--------+
        | Offset | Value | Bytes | Format |
        +--------+-------+-------+--------+
        |        |       |       | Sample |
        +--------+-------+-------+--------+

        TypeError occurred during pack operation:

        __init__() got an unexpected keyword argument 'inner_c'
        """
    )

    expect = RefCounts()
    try:
        pack(sample_extra_dict, Sample)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_bad_type():
    """Test value is bad type.

    branches: STRUCT_PACK:TYPE_BAD

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+--------+
        | Offset | Value | Bytes | Format |
        +--------+-------+-------+--------+
        |        |       |       | Sample |
        +--------+-------+-------+--------+

        TypeError occurred during pack operation:

        invalid value, 'Sample' pack expects an iterable of length 2, got a non-iterable
        """
    )

    expect = RefCounts()
    try:
        pack(0, Sample)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_bad_item():
    """Test value has bad member value.

    branches: STRUCT_PACK:ITEM_FAIL

    """
    expect_message = Baseline(
        """


        +--------+----------------+--------+-------+--------+
        | Offset | Access         | Value  | Bytes | Format |
        +--------+----------------+--------+-------+--------+
        |        |                |        |       | Sample |
        | 0      | [0] (.inner_a) | 0      | 00    | uint8  |
        |        | [1] (.inner_b) | 'junk' |       | uint8  |
        +--------+----------------+--------+-------+--------+

        TypeError occurred during pack operation:

        value type <class 'str'> not int-like (no to_bytes() method)
        """
    )

    expect = RefCounts()
    try:
        pack(sample_bad_item, Sample)
    except PackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect
