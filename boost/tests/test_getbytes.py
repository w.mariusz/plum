# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Copyright 2021 Daniel Mark Gass, see __about__.py for license information.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
"""Test reference count handling in boost getbytes() function.

FUTURE: test branch GETBYTES:BINFILE:READ_ALL when have an boosted type
        that is variable in size.

"""

# pylint: disable=no-self-use

from baseline import Baseline

from plum.exceptions import UnpackError
from plum.utilities import unpack
from plum.int import IntX

from .utils import RefCounts as RefCountsBase

uint8 = IntX("uint8", nbytes=1)

buf00 = b"\x00"
buf_null = b""


class BinFileNull:

    """Binary file with Read method that returns a consistent empty bytes object."""

    def read(self, size=-1):
        """Read bytes from buffer."""
        # pylint: disable=unused-argument
        return buf_null

    def seek(self, index):
        """Reset file position"""


class BinFileOK:

    """Binary file with Read method that returns a consistent object."""

    def __init__(self):
        self.index = 0

    def read(self, size=-1):
        """Read bytes from buffer."""
        if self.index or size < 0:
            retval = buf_null
        else:
            retval = buf00
            self.index += 1
        return retval

    def seek(self, index):
        """Reset file position"""
        self.index = index


class BinFileError:

    """Binary file with Read method that raises exception."""

    def read(self, size=-1):
        """Read bytes from buffer."""
        # pylint: disable=unused-argument
        raise RuntimeError("read method exception")

    def seek(self, index):
        """Reset file position"""


binfile_null = BinFileNull()
binfile_ok = BinFileOK()
binfile_error = BinFileError()


class RefCounts(RefCountsBase):

    """Python object reference count tracker."""

    def __init__(self):
        super().__init__(
            self,
            uint8,
            buf00=buf00,
            buf_null=buf_null,
            binfile_null=binfile_null,
            binfile_null_cls=BinFileNull,
            binfile_ok=binfile_ok,
            binfile_ok_cls=BinFileOK,
            binfile_error=binfile_error,
            binfile_error_cls=BinFileError,
        )


def test_bytes_ok():
    """Test with bytes buffer with sufficient data.

    branches: GETBYTES:BYTEBUF
    branches: GETBYTES:GOT_BYTES:OK

    """
    expect = RefCounts()
    retval = unpack(uint8, buf00)
    actual = RefCounts()

    assert actual == expect
    assert retval == 0


def test_bytes_shortage():
    """Test with bytes buffer with insufficient data.

    branches: GETBYTES:BYTEBUF:NBYTES_DIFFERS
    branches: GETBYTES:GOT_BYTES:SHORTAGE

    """
    expect_message = Baseline(
        """


        +--------+----------------------+-------+-------+
        | Offset | Value                | Bytes | Format|
        +--------+----------------------+-------+-------+
        |        | <insufficient bytes> |       | uint8 |
        +--------+----------------------+-------+-------+

        InsufficientMemoryError occurred during unpack operation:

        1 too few bytes to unpack uint8 (1 needed, only 0 available)
        """
    )

    expect = RefCounts()
    try:
        unpack(uint8, buf_null)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_binfile_ok():
    """Test with binary file with sufficient data.

    branches: GETBYTES:BINFILE:READ_NBYTES
    branches: GETBYTES:BINFILE:READ_OK
    branches: GETBYTES:GOT_BYTES:OK

    """
    expect = RefCounts()
    try:
        retval = unpack(uint8, binfile_ok)
    finally:
        binfile_ok.seek(0)
    actual = RefCounts()

    assert actual == expect
    assert retval == 0


def test_binfile_shortage():
    """Test with binary file with insufficient data.

    branches: GETBYTES:BINFILE:READ_NBYTES
    branches: GETBYTES:BINFILE:READ_OK
    branches: GETBYTES:GOT_BYTES:SHORTAGE

    """
    expect_message = Baseline(
        """


        +--------+----------------------+-------+-------+
        | Offset | Value                | Bytes | Format|
        +--------+----------------------+-------+-------+
        |        | <insufficient bytes> |       | uint8 |
        +--------+----------------------+-------+-------+

        InsufficientMemoryError occurred during unpack operation:

        1 too few bytes to unpack uint8 (1 needed, only 0 available)
        """
    )

    expect = RefCounts()
    try:
        unpack(uint8, binfile_null)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect


def test_binfile_read_error():
    """Test with binary file with unexpected read error.

    branches: GETBYTES:BINFILE:READ_NBYTES
    branches: GETBYTES:BINFILE:READ_ERROR
    branches: GETBYTES:ERROR

    """
    expect_message = Baseline(
        """


        +--------+-------+-------+-------+
        | Offset | Value | Bytes | Format|
        +--------+-------+-------+-------+
        |        |       |       | uint8 |
        +--------+-------+-------+-------+

        RuntimeError occurred during unpack operation:

        read method exception
        """
    )

    expect = RefCounts()
    try:
        unpack(uint8, binfile_error)
    except UnpackError as exc:
        exc_message = str(exc)
    actual = RefCounts()

    assert exc_message == expect_message
    assert actual == expect
