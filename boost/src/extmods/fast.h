#ifndef FAST_H
#define FAST_H

#include <Python.h>

typedef PyObject* (*fast_unpack_func)(void *fs, PyTypeObject* cls, Py_buffer *buffer, PyObject *memory, Py_ssize_t *offset, PyObject *parent);

typedef Py_ssize_t (*fast_pack_func)(void *fs, PyTypeObject* cls, PyObject *memory, Py_ssize_t offset, PyObject *item, PyObject *parent);

typedef struct {
    PyAsyncMethods am;
    Py_ssize_t nbytes;
    fast_unpack_func unpack;
    fast_pack_func pack;
} FastStruct;

typedef unsigned char * (*getbytes_func)(Py_buffer *buffer, PyObject *memory, Py_ssize_t *offset, Py_ssize_t *nbytes, PyObject **owner);

typedef PyObject* (*unpack_item_func)(PyObject* fmt, Py_buffer *buffer, PyObject *memory, Py_ssize_t *offset, PyObject *parent);

typedef Py_ssize_t (*pack_value_func)(PyTypeObject *cls, PyObject **memory, Py_ssize_t offset, PyObject *item, PyObject *parent);

typedef void (*fill_faststruct_func)(PyTypeObject *cls, FastStruct *);

typedef struct {
    fill_faststruct_func fill_faststruct;
    getbytes_func getbytes;
    pack_value_func pack_value;
    unpack_item_func unpack_item;
} FastUtilsStruct;


// #define dbgprintf(...) printf(__VA_ARGS__); printf("\n");

#ifndef dbgprintf
#define dbgprintf(...)
#endif

// #define tracef(...) printf(__VA_ARGS__); printf("\n");

#ifdef tracef
#define SET_REFCOUNTS(variable, object) variable=Py_REFCNT(object)
#define CMP_REFCOUNTS(variable, object) if ((object!=Py_None) && (variable!=Py_REFCNT(object))) {printf("refcnt(parent) mismanaged\n"); fflush(stdout); Py_UNREACHABLE();}
#else
#define SET_REFCOUNTS(variable, object)
#define CMP_REFCOUNTS(variable, object)
#define tracef(...)
#endif

#endif
