// included in utils.c

static Py_ssize_t pack_value(
        PyTypeObject *cls, PyObject **bytearray_retval, Py_ssize_t offset, PyObject *item, PyObject *parent) {
    tracef("PACK_VAL");

    FastStruct *fs = get_faststruct(cls);

    Py_INCREF(item);

    dbgprintf("  PACK_VAL: offset=%ld", offset);

    if (PyObject_TypeCheck(item, PlumView_Type)) {
        tracef("  PACK_VAL:VAL_IS_VIEW");
        PyObject *result = PyObject_CallMethod((PyObject*)item, "get", NULL);
        if (result) {
            tracef("  PACK_VAL:VAL_IS_VIEW:GET_SUCCESS");
            Py_DECREF(item);
            item = result;
        }
        else {
            tracef("  PACK_VAL:VAL_IS_VIEW:GET_FAIL");
            Py_DECREF(item);
            return -1;
        }
    }

    if (fs && (fs->nbytes >= 0)) {
        tracef("  PACK_VAL:FASTDATA_AND_FIXED");
        Py_ssize_t nbytes = offset + fs->nbytes;
        dbgprintf("  PACK_VAL: offset=%ld, fs->nbytes=%ld, nbytes=%ld", offset, fs->nbytes, nbytes);
        if (*bytearray_retval) {
            tracef("  PACK_VAL:FASTDATA_AND_FIXED:BUF_EXIST");
            dbgprintf("  PACK_VAL: check resize len(m)=%ld", PyByteArray_GET_SIZE(*bytearray_retval));
            if (nbytes > PyByteArray_GET_SIZE(*bytearray_retval)) {
                tracef("  PACK_VAL:FASTDATA_AND_FIXED:BUF_EXIST:RESIZING");
                if (PyByteArray_Resize(*bytearray_retval, nbytes)) {
                    Py_DECREF(item);
                    return -1;
                }
            }
        }
        else {
            tracef("  PACK_VAL:FASTDATA_AND_FIXED:BUF_CREATE");
            *bytearray_retval = PyByteArray_FromStringAndSize(NULL, nbytes);
            if (!*bytearray_retval) {
                Py_DECREF(item);
                return -1;
            }
        }
        if (fs->pack) {
            tracef("  PACK_VAL:FASTDATA_AND_FIXED:FAST_PACK");
            offset = fs->pack(fs, cls, *bytearray_retval, offset, item, parent);
            dbgprintf("  PACK_VAL: fast pack returned offset=%ld", offset);
        }
        else {
            tracef("  PACK_VAL:FASTDATA_AND_FIXED:SLOW_PACK");
            PyObject *result = PyObject_CallMethod(
                (PyObject*)cls, "__pack__", "OnOOO",
                *bytearray_retval, offset, parent, item, Py_None);
            if (result) {
                tracef("  PACK_VAL:FASTDATA_AND_FIXED:SLOW_PACK:SUCCESS");
                /* since we already know the new offset, ignore the returned offset */
                /* (to save time by avoiding conversion Python int to a Py_size_t) */
                Py_DECREF(result);
                offset = nbytes;
            } else {
                tracef("  PACK_VAL:FASTDATA_AND_FIXED:SLOW_PACK:FAIL");
                Py_DECREF(item);
                return -1;
            }
        }
    }
    else {
        tracef("  PACK_VAL:SLOW_OR_VAR");
        if (!*bytearray_retval) {
            tracef("  PACK_VAL:SLOW_OR_VAR:BUF_CREATE");
            *bytearray_retval = PyByteArray_FromStringAndSize(NULL, 0);
            if (!*bytearray_retval) {
                Py_DECREF(item);
                return -1;
            }
        }

        if (fs && fs->pack) {
            tracef("  PACK_VAL:SLOW_OR_VAR:FAST_PACK");
            offset = fs->pack(fs, cls, *bytearray_retval, offset, item, parent);
            dbgprintf("  PACK_VAL: fast pack returned offset=%ld", offset);
        }
        else {
            tracef("  PACK_VAL:SLOW_OR_VAR:SLOW_PACK");
            PyObject *result = PyObject_CallMethod(
                (PyObject*)cls, "__pack__", "OnOOO",
                *bytearray_retval, offset, parent, item, Py_None);
            if (result) {
                tracef("  PACK_VAL:SLOW_OR_VAR:SLOW_PACK:SUCCESS");
                offset = PyLong_AsSsize_t(result);
                dbgprintf("  PACK_VAL: slow pack returned offset=%ld", offset);
                Py_DECREF(result);
            }
            else {
                tracef("  PACK_VAL:SLOW_OR_VAR:SLOW_PACK:FAIL");
                Py_DECREF(item);
                return -1;
            }
        }
    }

    dbgprintf("  PACK_VAL: returning offset=%ld", offset);
    Py_DECREF(item);
    return offset;
}


static Py_ssize_t _pack_value_with_format(
        PyObject *fmt, PyObject **bytearray_retval, Py_ssize_t offset, PyObject *value, PyObject *parent) {
    tracef("PACK_VWF");

    if (PyObject_TypeCheck(fmt, PlumMeta_Type)) {
        tracef("  PACK_VWF:FMT_IS_PLUM");
        offset = pack_value((PyTypeObject*)fmt, bytearray_retval, offset, value, parent);
    }
    else if (PyTuple_Check(fmt) || PyList_Check(fmt)) {
        tracef("  PACK_VWF:FMT_IS_SEQ");
        if (PyTuple_Check(value) || PyList_Check(value)) {
            tracef("  PACK_VWF:FMT_IS_SEQ:VAL_IS_SEQ");
            Py_ssize_t num_items = PySequence_Size(fmt);
            if (PySequence_Size(value) != num_items) {
                tracef("  PACK_VWF:FMT_IS_SEQ:VAL_IS_SEQ:UNEQUAL_LEN");
                return -1;
            }
            for (Py_ssize_t i=0; i < num_items; i++) {
                dbgprintf("  PACK_VWF: packing item #%ld @ offset=%ld", i, offset);
                PyObject *item_fmt = PySequence_GetItem(fmt, i); /* issued reference */
                PyObject *item_value = PySequence_GetItem(value, i); /* issued reference */
                if ((!item_fmt) || (!item_value)) {
                    dbgprintf("  PACK_VWF: unexpected error indexing value or fmt");
                    Py_XDECREF(item_fmt);
                    Py_XDECREF(item_value);
                    return -1;
                }
                offset = _pack_value_with_format(item_fmt, bytearray_retval, offset, item_value, parent);
                Py_DECREF(item_fmt);
                Py_DECREF(item_value);
            }
        }
        else {
            tracef("  PACK_VWF:FMT_IS_SEQ:VAL_IS_NOT_SEQ");
            return -1;
        }
    }
    else if PyDict_Check(fmt) {
        tracef("  PACK_VWF:FMT_IS_DICT");
        if (PyDict_Check(value)) {
            tracef("  PACK_VWF:FMT_IS_DICT:VAL_IS_DICT");
            if (PyDict_Size(fmt) != PyDict_Size(value)) {
                tracef("  PACK_VWF:FMT_IS_DICT:VAL_IS_DICT:UNEQUAL_LEN");
                return -1;
            }
            PyObject *key;
            Py_ssize_t pos = 0;
            PyObject *item_fmt;

            while (PyDict_Next(fmt, &pos, &key, &item_fmt)) {
                dbgprintf("  PACK_VWF: packing item #%ld @ offset=%ld", pos, offset);
                PyObject *item_value = PyDict_GetItem(value, key); /* borrowed reference */
                if (!item_value) {
                    tracef("  PACK_VWF:FMT_IS_DICT:VAL_IS_DICT:MISSING_KEY");
                    return -1;
                }
                offset = _pack_value_with_format(item_fmt, bytearray_retval, offset, item_value, parent);
            }
        }
        else {
            tracef("  PACK_VWF:FMT_IS_DICT:VAL_NOT_DICT");
            return -1;
        }
    }
    else {
        tracef("  PACK_VWF:FMT_INVALID");
        return -1;
    }

    dbgprintf("  PACK_VWF: returning offset=%ld", offset);
    return offset;
}

static PyObject *pack(PyObject *module, PyObject *args, PyObject *kwds) {
    tracef("PACK");

    PyObject *bytearray_retval = NULL;
    PyObject *fmt=NULL;
    PyObject *fmt_arg=NULL;
    PyObject *fmt_kwd=NULL;
    PyObject *kwds_value=NULL;
    PyObject *args_slice=NULL;
    PyObject *args_value=NULL;
    PyObject *parent=Py_None;
    Py_ssize_t num_args = PyTuple_GET_SIZE(args);
    Py_ssize_t num_kwds = 0;
    int retry = 0;

    Py_ssize_t offset = -1;

    if (num_args) {
        tracef("  PACK:HAS_ARGS");
        fmt = fmt_arg = PyTuple_GET_ITEM(args, 0); /* borrowed reference */
        args_value = args_slice = PyTuple_GetSlice(args, 1, num_args); /* issued reference */
        if (!args_value) {
            goto error;
        }
    }
    else {
        tracef("  PACK:NO_ARGS");
        args_value = args;
    }

    if (kwds) {
        tracef("  PACK:HAS_KWDS");
        /* make copy in case of retry */
        kwds_value = PyDict_Copy(kwds);
        if (!kwds_value) {
            goto error;
        }
        fmt_kwd = _PyDict_Pop(kwds_value, fmt_string, NULL); /* transferred reference */
        if (fmt_kwd) {
            tracef("  PACK:HAS_KWDS:FMT_KWD");
            fmt = fmt_kwd;
        }
        else {
            tracef("  PACK:HAS_KWDS:NO_FMT_KWD");
            PyErr_Clear();
        }
        num_kwds = PyDict_Size(kwds_value);
        dbgprintf("  PACK: num_kwds=%ld", num_kwds);
    }

    if (fmt_arg && fmt_kwd) {
        tracef("  PACK:HAS_DUP_FMT");
        PyErr_SetString(PyExc_TypeError, "pack() got multiple values for argument 'fmt'");
        goto error;
    }

    if (!fmt) {
        tracef("  PACK:NO_FMT");
        PyErr_SetString(PyExc_TypeError, "pack() missing 1 required positional argument: 'fmt'");
        goto error;
    }

    retry = 1;

    if (PyObject_TypeCheck(fmt, PlumMeta_Type)) {
        tracef("  PACK:FMT_IS_PLUM");
        if (num_kwds || PyTuple_GET_SIZE(args_value) != 1) {
            tracef("  PACK:FMT_IS_PLUM:EXTRA_VALUES");
            goto error;
        }
        offset = pack_value((PyTypeObject*)fmt, &bytearray_retval, 0, PyTuple_GET_ITEM(args_value, 0), parent);
    }
    else if (PyTuple_Check(fmt) || PyList_Check(fmt)) {
        tracef("  PACK:FMT_IS_SEQ");
        if (num_kwds) {
            tracef("  PACK:FMT_IS_SEQ:WITH_KWDS");
            goto error;
        }
        offset = _pack_value_with_format(fmt, &bytearray_retval, 0, args_value, parent);
    }
    else if PyDict_Check(fmt) {
        tracef("  PACK:FMT_IS_DICT");
        if (PyTuple_GET_SIZE(args_value)) {
            tracef("  PACK:FMT_IS_DICT:WITH_ARGS");
            goto error;
        }
        else if (kwds_value) {
            tracef("  PACK:FMT_IS_DICT:PACKING_KWDS");
            offset = _pack_value_with_format(fmt, &bytearray_retval, 0, kwds_value, parent);
        }
        else if (PyDict_Size(fmt)) {
            tracef("  PACK:FMT_IS_DICT:NO_VALUES");
            goto error;
        }
        else {
            /* do nothing */
            tracef("  PACK:FMT_IS_DICT:BUT_EMPTY");
            PyObject *empty_dict = PyDict_New();
            if (!empty_dict) {
                goto error;
            }
            offset = _pack_value_with_format(fmt, &bytearray_retval, 0, empty_dict, parent);
            Py_DECREF(empty_dict);
        }
    }
    else {
        tracef("  PACK:INVALID_FMT");
        goto error;
    }

    dbgprintf("  PACK:offset=%ld", offset);

    Py_XDECREF(args_slice);
    Py_XDECREF(fmt_kwd);
    Py_XDECREF(kwds_value);

    if (offset < 0) {
        tracef("  PACK:FAILED_PACK");
        goto error;
    }

    if (!bytearray_retval) {
        tracef("  PACK:NO_VALUES_PACKED");
        bytearray_retval = PyByteArray_FromStringAndSize(NULL, 0);
    }

    return bytearray_retval;

error:
    tracef("  PACK:ERROR");
    Py_XDECREF(args_slice);
    Py_XDECREF(fmt_kwd);
    Py_XDECREF(kwds_value);
    Py_XDECREF(bytearray_retval);

    if (retry) {
        tracef("  PACK:ERROR:RETRY");
        /* do it over to include dump in exception message */
        /* (performance is not a concern at this point) */
        PyErr_Clear();

        bytearray_retval = PyObject_Call(plum_pack_and_dump, args, kwds);

        if (bytearray_retval) {
            dbgprintf("  PACK:implementation error");
            PyErr_SetNone(PyErr_ImplementationError);
            Py_DECREF(bytearray_retval);
        }
    }

    dbgprintf("  PACK:return=NULL");
    return NULL;
};
