// included in utils.c

static PyObject *unpack_plum(PyTypeObject* cls, Py_buffer *buffer, PyObject *buffer_arg, Py_ssize_t *offset, PyObject *parent) {
    tracef("UNPACK_PLUM");

    FastStruct *fs = get_faststruct(cls);
    PyObject *item;

    if (fs && fs->unpack) {
        tracef("  UNPACK_PLUM:FAST");
        Py_ssize_t references;
        references = references;  // eliminate compiler warning when debugging off
        dbgprintf("  UNPACK_PLUM: refcnt(parent)=%ld (before unpack)", Py_REFCNT(parent));
        SET_REFCOUNTS(references, parent);
        item = fs->unpack(fs, cls, buffer, buffer_arg, offset, parent);
        dbgprintf("  UNPACK_PLUM: refcnt(parent)=%ld (after unpack)", Py_REFCNT(parent));
        CMP_REFCOUNTS(references, parent);
    }
    else {
        tracef("  UNPACK_PLUM:SLOW");
        dbgprintf("  UNPACK_PLUM: refcnt(parent)=%ld, refcnt(None)=%ld (before slow unpack)", Py_REFCNT(parent), Py_REFCNT(Py_None));
        PyObject *result = PyObject_CallMethod((PyObject*)cls, "__unpack__", "OnOO", buffer_arg, *offset, parent, Py_None);
        dbgprintf("  UNPACK_PLUM: refcnt(parent)=%ld, refcnt(None)=%ld (after slow unpack)", Py_REFCNT(parent), Py_REFCNT(Py_None));
        if (result) {
            tracef("  UNPACK_PLUM:SLOW:SUCCEED");
            if (PyTuple_Size(result) == 2) {
                tracef("  UNPACK_PLUM:SLOW:SUCCEED:TUPLE_OK");
                item = PyTuple_GET_ITEM(result, 0);
                *offset = PyLong_AsSsize_t(PyTuple_GET_ITEM(result, 1));
                if (*offset < 0) {
                    tracef("  UNPACK_PLUM:SLOW:SUCCEED:TUPLE_OK:OFFSET_BAD");
                    item = NULL;
                }
                else {
                    tracef("  UNPACK_PLUM:SLOW:SUCCEED:TUPLE_OK:OFFSET_OK");
                    Py_INCREF(item);
                }
            }
            else {
                tracef("  UNPACK_PLUM:SLOW:TUPLE_BAD");
                dbgprintf("  UNPACK_PLUM: cls.__unpack__() had result but wrong type/size");
                /* raise TypeError('__unpack__() had result but wrong type/size') */
                PyErr_SetNone(PyExc_TypeError);
                item = NULL;
            }
            Py_DECREF(result);
        }
        else {
            tracef("  UNPACK_PLUM:SLOW:ERROR");
            item = NULL;
        }
    }

    return item;
};

static PyObject *unpack_list(PyObject* types, Py_buffer *buffer, PyObject *buffer_arg, Py_ssize_t *offset);
static PyObject *unpack_tuple(PyObject* types, Py_buffer *buffer, PyObject *buffer_arg, Py_ssize_t *offset);
static PyObject *unpack_dict(PyObject* types, Py_buffer *buffer, PyObject *buffer_arg, Py_ssize_t *offset);

static PyObject *unpack_item(PyObject *fmt, Py_buffer *buffer, PyObject *buffer_arg, Py_ssize_t *offset, PyObject *parent) {

    tracef("UNPACK_ITEM");

    PyObject* item = NULL;

    if (PyObject_TypeCheck(fmt, PlumMeta_Type)) {
        tracef("  UNPACK_ITEM:PLUM");
        item = unpack_plum((PyTypeObject*)fmt, buffer, buffer_arg, offset, parent);
    }
    else if (PyTuple_Check(fmt)) {
        tracef("  UNPACK_ITEM:TUPLE");
        item = unpack_tuple(fmt, buffer, buffer_arg, offset);
    }
    else if (PyList_Check(fmt)) {
        tracef("  UNPACK_ITEM:LIST");
        item = unpack_list(fmt, buffer, buffer_arg, offset);
    }
    else if PyDict_Check(fmt) {
        tracef("  UNPACK_ITEM:DICT");
        item = unpack_dict(fmt, buffer, buffer_arg, offset);
    }
    else {
        tracef("  UNPACK_ITEM:BAD_FMT");
    }

    return item;
}

static PyObject *unpack_list(PyObject* types, Py_buffer *buffer, PyObject *buffer_arg, Py_ssize_t *offset) {

    tracef("UNPACK_LIST");

    Py_ssize_t numitems = PyList_GET_SIZE(types);

    dbgprintf("  UNPACK_LIST: numitems = %ld", numitems);

    PyObject *items = PyList_New(numitems);
    if (items == NULL) {
        return NULL;
    }

    for (Py_ssize_t i=0; i < numitems; i++) {
        tracef("  UNPACK_LIST:FOR");
        PyObject *cls = PyList_GET_ITEM(types, i);

        PyObject *item = unpack_item(cls, buffer, buffer_arg, offset, Py_None);

        if (item == NULL) {
            tracef("  UNPACK_LIST:FOR:ITEM_BAD");
            dbgprintf("  UNPACK_LIST: item did not unpack, throwing list away");
            Py_DECREF(items);
            items = NULL;
            break;
        }
        else {
            tracef("UNPACK_LIST:FOR:ITEM_OK");
            PyList_SET_ITEM(items, i, item);
        }
    }

    return items;
};

static PyObject *unpack_tuple(PyObject* types, Py_buffer *buffer, PyObject *buffer_arg, Py_ssize_t *offset) {

    tracef("UNPACK_TUPLE");

    Py_ssize_t numitems = PyTuple_GET_SIZE(types);
    dbgprintf("  UNPACK_TUPLE:numitems = %ld", numitems);

    PyObject *items = PyTuple_New(numitems);
    if (items == NULL) {
        return NULL;
    }

    for (Py_ssize_t i=0; i < numitems; i++) {
        tracef("  UNPACK_TUPLE:FOR");
        PyObject *cls = PyTuple_GET_ITEM(types, i);

        dbgprintf("  UNPACK_TUPLE: unpacking item %ld", i);
        PyObject *item = unpack_item(cls, buffer, buffer_arg, offset, Py_None);

        if (item == NULL) {
            tracef("  UNPACK_TUPLE:FOR:ITEM_BAD");
            dbgprintf("  UNPACK_TUPLE: item did not unpack, throwing tuple away");
            Py_DECREF(items);
            items = NULL;
            break;
        }
        else {
            tracef("  UNPACK_TUPLE:FOR:ITEM_OK");
            PyTuple_SET_ITEM(items, i, item);
        }
    }

    return items;
};

static PyObject *unpack_dict(PyObject* types, Py_buffer *buffer, PyObject *buffer_arg, Py_ssize_t *offset) {

    tracef("UNPACK_DICT");

    PyObject *items = PyDict_New();

    if (items == NULL) {
        return NULL;
    }

    PyObject *key, *fmt;
    Py_ssize_t pos = 0;

    while (PyDict_Next(types, &pos, &key, &fmt)) {
        tracef("  UNPACK_DICT:WHILE");
        dbgprintf("  UNPACK_DICT:unpacking item %ld", pos - 1);
        PyObject *item = unpack_item(fmt, buffer, buffer_arg, offset, Py_None);

        if (item) {
            tracef("  UNPACK_DICT:WHILE:ITEM_OK");
            if (PyDict_SetItem(items, key, item)) {
                dbgprintf("  UNPACK_DICT:item did not insert into dict, clearing dict");
                Py_DECREF(items);
                return NULL;
            }
            Py_DECREF(item); /* PyDict_SetItem() doesn't steal reference */
        }
        else {
            tracef("  UNPACK_DICT:WHILE:ITEM_BAD");
            Py_DECREF(items);
            return NULL;
       }
    }

    return items;
};

static PyObject *unpack_from_buffer(PyObject *fmt, PyObject *buffer_arg, PyObject *offset_arg,
                                    int check_excess, PyObject *parent, Py_ssize_t *final_offset) {
    Py_buffer _buffer;
    Py_buffer *buffer;
    Py_ssize_t offset=0;

    tracef("UNPACK_FB");

    if (PyObject_GetBuffer(buffer_arg, &_buffer, PyBUF_SIMPLE)) {
        tracef("  UNPACK_FB:BINFILE");
        /* not bytes-like must be binary file */
        PyErr_Clear();

        buffer = NULL;

        if (offset_arg == Py_None) {
            tracef("  UNPACK_FB:BINFILE:TELL_OFF");
            PyObject *tell_value = PyObject_CallMethod(buffer_arg, "tell", NULL);
            if (!tell_value) {
                tracef("  UNPACK_FB:BINFILE:TELL_OFF:FAIL");
                return NULL;
            }
            tracef("  UNPACK_FB:BINFILE:TELL_OFF:SUCCEED");
            offset = PyLong_AsSsize_t(tell_value);
            dbgprintf("  UNPACK_FB: tell offset = %ld", offset);
            Py_DECREF(tell_value);
            if (offset == -1) {
                return NULL;
            }
        }
        else {
            tracef("  UNPACK_FB:BINFILE:OFF_ARG");

            offset = PyLong_AsSsize_t(offset_arg);
            if (offset == -1) {
                tracef("  UNPACK_FB:BINFILE:OFF_ARG:BAD");
                return NULL;
            }
            dbgprintf("  UNPACK_FB: offset=%ld", offset);

            PyObject *none_value = PyObject_CallMethod(buffer_arg, "seek", "O", offset_arg, NULL);
            Py_XDECREF(none_value);
            if (!none_value) {
                tracef("  UNPACK_FB:BINFILE:OFF_ARG:SEEK_FAIL");
                return NULL;
            }
            tracef("  UNPACK_FB:BINFILE:OFF_ARG:SEEK_OK");
        }
    }
    else {
        tracef("  UNPACK_FB:BYTEBUF");
        buffer = &_buffer;
        if (offset_arg == Py_None) {
            tracef("  UNPACK_FB:BYTEBUF:DEFAULT_OFFSET");
            offset = 0;
        }
        else {
            tracef("  UNPACK_FB:BYTEBUF:OFFSET_ARG");
            offset = PyLong_AsSsize_t(offset_arg);
            if (offset == -1) {
                tracef("  UNPACK_FB:BYTEBUF:OFFSET_ARG:BAD");
                PyBuffer_Release(buffer);
                return NULL;
            }
            tracef("  UNPACK_FB:BYTEBUF:OFFSET_ARG:OK");
            dbgprintf("  UNPACK_FB: offset=%ld", offset);
        }
    }

    *final_offset = offset;  /* in case of re-do */

    PyObject *values = unpack_item(fmt, buffer, buffer_arg, final_offset, Py_None);

    if (values && check_excess) {
        tracef("  UNPACK_FB:CHECK_EXCESS");
        Py_ssize_t num_excess_bytes;
        if (buffer) {
            tracef("  UNPACK_FB:CHECK_EXCESS:BYTEBUF");
            num_excess_bytes = buffer->len - *final_offset;
        }
        else {
            tracef("  UNPACK_FB:CHECK_EXCESS:BINFILE");
            PyObject *leftover = PyObject_CallMethod(buffer_arg, "read", NULL);
            if (leftover) {
                tracef("  UNPACK_FB:CHECK_EXCESS:BINFILE:READ_OK");
                num_excess_bytes = Py_SIZE(leftover);
                Py_DECREF(leftover);
            }
            else {
                tracef("  UNPACK_FB:CHECK_EXCESS:BINFILE:READ_FAIL");
                num_excess_bytes = 1;  /* recycle normal ref count / error handling */
            }
        }

        dbgprintf("  UNPACK_FB: num_excess_bytes = %ld", num_excess_bytes);

        if (num_excess_bytes) {
            tracef("  UNPACK_FB:CHECK_EXCESS:ERROR");
            Py_DECREF(values);
            values = NULL;
        }
    }

    if (buffer) {
        tracef("  UNPACK_FB:RELEASE_BUF");
        PyBuffer_Release(buffer);
    }

    if (!values) {
        tracef("  UNPACK_FB:ERROR");
        /* prepare to re-do operation and generate exception using plum-py utility */
        /* (speed no longer important) */
        PyErr_Clear();
        if (!buffer) {
            tracef("  UNPACK_FB:ERROR:SEEK");
            /* rewind binary file to where we started */
            PyObject *none_value = PyObject_CallMethod(buffer_arg, "seek", "n", offset, NULL);
            Py_XDECREF(none_value);
        }
    }

    return values;
}

static PyObject *unpack_from(PyObject *module, PyObject *args, PyObject *kwds) {
    PyObject *fmt;
    PyObject *buffer_arg;
    PyObject *offset_arg = Py_None;

    tracef("UNPACK_FROM");

    static char *kwlist[] = {"fmt", "buffer", "offset", NULL};

    if (! PyArg_ParseTupleAndKeywords(args, kwds, "OO|O", kwlist, (PyObject**) &fmt, &buffer_arg, &offset_arg)) {
        tracef("  UNPACK_FROM:KW_FAIL");
        return NULL;
    }

    Py_ssize_t final_offset = 0;
    PyObject *values = unpack_from_buffer(fmt, buffer_arg, offset_arg, 0, Py_None, &final_offset);

    if (!values) {
        tracef("  UNPACK_FROM:ERROR");
        /* re-do operation and generate exception using plum-py utility */
        /* (speed no longer important) */
        PyErr_Clear();
        values = PyObject_CallFunction(plumunpack_from_buffer_and_dump, "OOO", (PyObject*) fmt, buffer_arg, offset_arg);
        if (values) {
            PyErr_SetNone(PyErr_ImplementationError);
            Py_DECREF(values);
            return NULL;
        }
    }

    dbgprintf("unpack_from() returning values %p", values);

    return values;
};


static PyObject *unpack(PyObject *self, PyObject *args)
{
    PyObject *fmt;  /* borrowed ref */
    PyObject *buffer_arg;  /* borrowed ref */

    tracef("UNPACK");

    if (!PyArg_UnpackTuple(args, "unpack", 2, 2, &fmt, &buffer_arg)) {
        tracef("  UNPACK:POS_FAIL");
        return NULL;
    }

    Py_ssize_t final_offset = 0;
    PyObject *values = unpack_from_buffer(fmt, buffer_arg, zero, 1, Py_None, &final_offset);

    if (!values) {
        tracef("  UNPACK:ERROR");
        /* re-do operation and generate exception using plum-py utility */
        /* (speed no longer important) */
        PyErr_Clear();
        values = PyObject_CallFunction(plum_unpack_and_dump, "OO", (PyObject*) fmt, buffer_arg);

        if (values) {
            PyErr_SetNone(PyErr_ImplementationError);
            Py_DECREF(values);
            values = NULL;
        }
    }

    return values;
};
