// included in utils.c

static unsigned char *getbytes(Py_buffer *buffer, PyObject*buffer_arg, Py_ssize_t *offset, Py_ssize_t *nbytes, PyObject **bytes_object) {
    tracef("GETBYTES");
    unsigned char *src = NULL;
    *bytes_object = NULL;
    Py_ssize_t requested_nbytes = *nbytes;

    if (buffer) {
        tracef("  GETBYTES:BYTEBUF");
        /* implementation for buffer (e.g. bytes, bytearray, etc.) */
        src = ((unsigned char *)buffer->buf) + *offset;

        if ((*nbytes < 0) || ((*offset + *nbytes) > buffer->len)) {
            tracef("  GETBYTES:BYTEBUF:NBYTES_DIFFERS");
            *nbytes = buffer->len - *offset;
        }
    }
    else {
        tracef("  GETBYTES:BINFILE");
        /* implementation for binary file (e.g. io.BytesIO) */

        if (*nbytes < 0) {
            tracef("  GETBYTES:BINFILE:READ_ALL");
            *bytes_object = PyObject_CallMethod(buffer_arg, "read", NULL);
        }
        else {
            tracef("  GETBYTES:BINFILE:READ_NBYTES");
            *bytes_object = PyObject_CallMethod(buffer_arg, "read", "n", *nbytes);
        }

        if (*bytes_object) {
            tracef("  GETBYTES:BINFILE:READ_OK");
            src = (unsigned char *) PyBytes_AS_STRING(*bytes_object);
            *nbytes = PyBytes_Size(*bytes_object);
        }
        else {
            tracef("  GETBYTES:BINFILE:READ_ERROR");
            *nbytes = -1;  /* indicate error */
        }
    }

    if (*nbytes >= 0) {
        tracef("  GETBYTES:GOT_BYTES");
        if ((requested_nbytes < 0) || (requested_nbytes == *nbytes)) {
            tracef("  GETBYTES:GOT_BYTES:OK");
            *offset += *nbytes;
        }
        else {
            tracef("  GETBYTES:GOT_BYTES:SHORTAGE");
            *nbytes = -1; /* indicate error */
            src = NULL;
        }
    }
    else {
        tracef("  GETBYTES:ERROR");
        src = NULL;
    }

    return src;
}
