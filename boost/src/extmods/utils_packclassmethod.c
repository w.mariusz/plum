// included in utils.c

/* pack class method object */

/* A pack class method receives the class as implicit first argument,
   just like an instance method receives the instance.
   To declare a pack class method, use this idiom:

     class C:
         @PackMethod
         def __pack__(cls, arg1, arg2, ...):
             ...

*/

typedef struct {
    PyObject_HEAD
    PyObject *pcm_callable;
    PyObject *pcm_dict;
} packclassmethod;

static void
pcm_dealloc(packclassmethod *pcm)
{
    // FUTURE - is this needed?
    // _PyObject_GC_UNTRACK((PyObject *)pcm);
    Py_XDECREF(pcm->pcm_callable);
    Py_XDECREF(pcm->pcm_dict);
    Py_TYPE(pcm)->tp_free((PyObject *)pcm);
}

static int
pcm_traverse(packclassmethod *pcm, visitproc visit, void *arg)
{
    Py_VISIT(pcm->pcm_callable);
    Py_VISIT(pcm->pcm_dict);
    return 0;
}

static int
pcm_clear(packclassmethod *pcm)
{
    Py_CLEAR(pcm->pcm_callable);
    Py_CLEAR(pcm->pcm_dict);
    return 0;
}


static PyObject *
pcm_descr_get(PyObject *self, PyObject *obj, PyObject *type)
{
    packclassmethod *pcm = (packclassmethod *)self;

    if (pcm->pcm_callable == NULL) {
        PyErr_SetString(PyExc_RuntimeError,
                        "uninitialized packclassmethod object");
        return NULL;
    }
    if (type == NULL)
        type = (PyObject *)(Py_TYPE(obj));
    return PyMethod_New(self, type);
}

static PyObject *
pcm_call(PyObject *self, PyObject *args, PyObject *kwds)
{
    packclassmethod *pcm = (packclassmethod *)self;

    tracef("DUNDERPACKMETHOD_CALL");

    PyObject *cls;
    PyObject *buffer;
    Py_ssize_t offset;
    PyObject *parent;
    PyObject *value;
    PyObject *dump;
    PyObject *result;

    static char *kwlist[] = {"cls", "buffer", "offset", "parent", "value", "dump", NULL};
    if (! PyArg_ParseTupleAndKeywords(args, kwds, "OOnOOO", kwlist, &cls, &buffer, &offset, &parent, &value, &dump)) {
        tracef("  DUNDERPACKMETHOD_CALL:KEYWORDS_FAIL");
        return NULL;
    }

    if (dump == Py_None) {
        offset = pack_value((PyTypeObject *)cls, &buffer, offset, value, parent);
        if (offset < 0) {
            result = NULL;
        }
        else {
            result = PyLong_FromSsize_t(offset);
        }
    }
    else {
        result = PyObject_CallFunction(pcm->pcm_callable, "OOnOOO", cls, buffer, offset, parent, value, dump);
    }

    return result;
}

static int
pcm_init(PyObject *self, PyObject *args, PyObject *kwds)
{
    packclassmethod *pcm = (packclassmethod *)self;
    PyObject *callable;

    if (!_PyArg_NoKeywords("packclassmethod", kwds))
        return -1;
    if (!PyArg_UnpackTuple(args, "packclassmethod", 1, 1, &callable))
        return -1;
    Py_INCREF(callable);
    Py_XSETREF(pcm->pcm_callable, callable);
    return 0;
}

static PyMemberDef pcm_memberlist[] = {
    {"__func__", T_OBJECT, offsetof(packclassmethod, pcm_callable), READONLY},
    {NULL}  /* Sentinel */
};

static PyObject *
pcm_get___isabstractmethod__(packclassmethod *pcm, void *closure)
{
    int res = _PyObject_IsAbstract(pcm->pcm_callable);
    if (res == -1) {
        return NULL;
    }
    else if (res) {
        Py_RETURN_TRUE;
    }
    Py_RETURN_FALSE;
}

static PyGetSetDef pcm_getsetlist[] = {
    {"__isabstractmethod__",
     (getter)pcm_get___isabstractmethod__, NULL,
     NULL,
     NULL},
    {"__dict__", PyObject_GenericGetDict, PyObject_GenericSetDict, NULL, NULL},
    {NULL} /* Sentinel */
};

PyDoc_STRVAR(packclassmethod_doc,
"packclassmethod(function) -> method\n\
\n\
Convert a function to be a pack class method.\n\
\n\
A pack class method receives the class as implicit first argument,\n\
just like an instance method receives the instance.\n\
To declare a pack class method, use this idiom:\n\
\n\
  class C:\n\
      @packclassmethod\n\
      def f(cls, arg1, arg2, ...):\n\
          ...\n\
\n\
It can be called either on the class (e.g. C.f()) or on an instance\n\
(e.g. C().f()).  The instance is ignored except for its class.\n\
If a class method is called for a derived class, the derived class\n\
object is passed as the implied first argument.");

PyTypeObject PackClassMethod_Type = {
    PyVarObject_HEAD_INIT(&PyType_Type, 0)
    .tp_name = "packclassmethod",
    .tp_basicsize = sizeof(packclassmethod),
    .tp_dealloc = (destructor)pcm_dealloc,
    .tp_call = (ternaryfunc)pcm_call,
    .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE | Py_TPFLAGS_HAVE_GC,
    .tp_doc = packclassmethod_doc,
    .tp_traverse = (traverseproc)pcm_traverse,
    .tp_clear = (inquiry)pcm_clear,
    .tp_members = pcm_memberlist,
    .tp_getset = pcm_getsetlist,
    .tp_descr_get = pcm_descr_get,
    .tp_dictoffset = offsetof(packclassmethod, pcm_dict),
    .tp_init = pcm_init,
    .tp_alloc = PyType_GenericAlloc,
    .tp_new = PyType_GenericNew,
    .tp_free = PyObject_GC_Del,
};
