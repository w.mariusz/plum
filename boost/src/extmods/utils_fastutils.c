// included in utils.c

static void fill_faststruct(PyTypeObject *cls, FastStruct *fs) {

    cls->tp_as_async = (PyAsyncMethods*) fs;
    fs->am.am_await = NULL;
    fs->am.am_aiter = NULL;
    fs->am.am_anext = async_method;
    fs->nbytes = -1;
    fs->unpack = NULL;
    fs->pack = NULL;
};

static FastUtilsStruct fastutils = {
    fill_faststruct,
    getbytes,
    pack_value,
    unpack_item,
};


static PyObject *
c_api_get_fastutils_pointer(PyObject *self, PyObject *Py_UNUSED(ignored))
{
    FastUtilsStruct *ptr = &fastutils;
    return PyBytes_FromStringAndSize((char *) &ptr, sizeof(ptr));
}

