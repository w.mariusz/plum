####################
[plum] Release Notes
####################

Versions increment per `semver <http://semver.org/>`_ (except for alpha/beta versions).

**********************************************************************
Beta (0.X.Y) Versions (X increments on backwards incompatible changes)
**********************************************************************

+ 0.7.3 2022-Feb-03

    - Previously `BytesX` transform did not show an entry in the dump
      for zero length bytes. Now it adds an entry to the dump showing
      access as `[0:0]` and a value of `b''`.
    - Previously the `bitfield()` property did not accept strings with
      newlines.

+ 0.7.2 2021-Nov-07

    - Support packing ``bytearray()`` with ``ItemsX`` transforms.
    - Previously when an exception occurred during pack/unpack operation and the
      dump records were half baked, creating dump representation for exception
      message resulted in a different error and didn't allow the dump to be
      seen and used.
    - Add ``AttrDict`` and ``AttrDictX`` for packing/unpacking dictionaries
      where members are accessible via attribute syntax.

+ 0.7.1 2021-Nov-02

    - Support ``BitFields`` usage as ``typ`` in structure ``bitfield_member()``.

+ 0.7.0 2021-Jun-27

    - Backwards Incompatible
        - Change ``BitFields`` and ``Structure`` default behavior to automatically place
          bitfields from most significant to least significant position when lsb not
          specified. Add ``fieldorder`` metaclass keyword argument to support old behavior
          (e.g. specify ``fieldorder="least_to_most"``).
        - Change ``bitfield()`` and ``bitfield_member()`` to always default to ``int`` for
          ``typ`` (previously it defaulted to the type annotation if present, otherwise to
          ``int``; this change is in preparation for supporting Python 3.10)

    - Backwards Compatible
        - Add ``SizedX`` transform properties (e.g. ``fmt``, ``size_fmt``, etc.)
        - Fix ``BitFields`` to allow `nbytes` to allow any size (previously it had to be
          a power of two).

+ 0.6.0 2021-May-20

    - Add support to ArrayX for packing/unpacking an array with a dims header.
    - Add SizedX transform for packing/unpacking an object with a size header.
    - Fix anomalous NameError exception which occurred when unpacking a structure
      member which has a default and set to "readonly" (occurred for cases where
      the eval of the string representation of default value is not the same
      object as the default value itself).

+ 0.5.0 2021-May-10

    - This beta release contains many breaking changes! This release note
      entry attempts to summarize most breaking changes. Please read the
      extensively rewritten documentation to assess in detail what has changed.

    - Python 3.6 support dropped.

    - Changed most plum constructs (e.g. `Int`, `Float`, etc.) from being
      a data store type (with transform properties) into a simple
      transform type (e.g. `IntX`, `FloatX`, etc.). Instead of subclassing
      to obtain a custom transform, now instantiate the transform.

    - `ByteArray` data store eliminated and replaced with `BytesX` transform.

    - Added `ItemsX` transform for specifying dict, list, tuple formats.

    - Structure & BitFields changes
        - Bitfield members `cls` argument name changed to `typ`.
        - Member `cls` argument name changed to `fmt`.
        - Type annotations no longer used for the member format transform
          (previously if `cls` was unspecified, the annotation was used).
        - Pack operations no longer accept dictionaries.
        - Reduced number of member definition classes (same abilities exist,
          but features were added to standard `Member` to accomodate them).
        - `pack()` now only usable as a class method. `ipack()` introduced
          for usage on instances.

    - Pack operations now return `bytes` instead of `bytearray`.

    - Unpack operations no longer support File I/O as input buffer.

    - `pack()` and `unpack()` functions/methods offer more flexibility for
      specifying data transform format.

    - `pack_into()` eliminated.

    - `unpack_from()` eliminated (use `Buffer` construct instead).


+ 0.4.0 2020-Aug-28

    - Support following boost performance improvements:

        - Improve performance of ``pack()`` and ``unpack()`` method calls for
          structure and integer based types.
        - Improve performance of calls to ``__pack__`` and ``__unpack__``
          for structure and integer and based types.

+ 0.3.1 2020-Apr-13

    - Add support for passing a factory function as the ``cls`` argument
      for following variable member definitions:

        + ``VariableDimsMember``
        + ``VariableSizeMember``

    - Improve structure representation of enumeration members.

+ 0.3.0 2020-Mar-31

    - Add ``plum.items`` module for collections of typed items for data
      packing applications where the item structure is not predefined.
    - Change ``plum.structure.Structure`` behaviors as follows:
        - Remove support for accepting typed items (use ``plum.items``
          instead).
        - Block subclasses from redefining members when inheriting
          from a class with members already defined.
        - Generate ``__init__`` method only when not overridden
          (previously it generated ``__init__`` only if an override
          was not present in the new structure class, now it also
          checks the base classes).
        - Facilitate subclassing structure base class without
          defining members as well as subclassing a structure that
          has defined members.
        - Eliminate restrictions on mutating structures (e.g. ``append()``,
          ``extend()``, etc.)
    - Add support to ``ByteArray`` to restrict instances to a specific
      size during unpacking (add ``nbytes`` parameter to ``__unpack__``).
    - Simplifications
        - Remove ``cls`` argument in ``getbytes()`` function (backward
          incompatible change).
        - Eliminate need to set ``dump.cls`` in ``__pack__`` and
          ``__unpack__`` methods (it is now the responsibility of
          those calling ``add_record()`` dump method).

+ 0.2.0 2020-Mar-11

    - Fix ``IpV4Address`` iteration order.
        - Iteration/index/accepted iterables now ordered from least
          significant byte to most. (Previously it followed byte
          order of the class.)
    - Support ``source`` metaclass argument when subclassing
      ``Enum`` and ``Flag`` enumeration types. Argument facilitates
      inheriting enumeration members.

