.. automodule:: plum.float

.. include:: ../alias.txt

#############################
[plum.float] Module Reference
#############################

The :mod:`plum.float` module provides the |FloatX| transform for converting
a floating point number into bytes and bytes into a floating point number.
This reference page demonstrates creating and using a |FloatX| transform as
well as provides API details.

.. contents::
    :local:

The examples shown on this page require the following setup:

    >>> from plum.array import ArrayX
    >>> from plum.float import FloatX
    >>> from plum.utilities import pack, unpack

*********
Basic Use
*********

The |FloatX| transform accepts the following arguments:

    :name: transform name (for representations)
    :nbytes: format size in bytes, ``2``, ``4``, or ``8`` (half, single, double precision)
    :byteorder: ``"big"`` or ``"little"`` (default)

For example:

    >>> float16 = FloatX("float16", nbytes=2, byteorder="big")
    >>> float32 = FloatX("float16", nbytes=4, byteorder="big")

Use the transform to specify a format when using the |pack()| and |unpack()| utility
functions or when using other high level transforms:

    >>> fmt = [float16, float32]
    >>>
    >>> bindata = pack([1, -2], fmt)
    >>> bindata.hex()
    '3c00c0000000'
    >>>
    >>> unpack(fmt, bindata)
    [1.0, -2.0]
    >>>
    >>> array2x2 = ArrayX("array2x2", fmt=float16, dims=(2, 2))
    >>>
    >>> bindata = pack([[1, 2], [3, 4]], fmt=array2x2)
    >>> bindata.hex()
    '3c00400042004400'
    >>>
    >>> unpack(array2x2, bindata)
    [[1.0, 2.0], [3.0, 4.0]]

.. Tip::
    Use ``sys.byteorder`` as the |FloatX| transform ``byteorder`` argument to
    get the same byte order as the architecture of the machine your script
    is running on.

*************
API Reference
*************

.. autoclass:: FloatX

    .. autoattribute:: byteorder

    .. autoattribute:: name

    .. autoattribute:: nbytes

    .. automethod:: pack

    .. automethod:: pack_and_dump

    .. automethod:: unpack

    .. automethod:: unpack_and_dump
