.. automodule:: plum.buffer

.. include:: ../alias.txt

##############################
[plum.buffer] Module Reference
##############################

The :mod:`plum.buffer` module provides the |Buffer| class for the purposes
of incrementally unpacking Python objects out of a bytes sequence.
See the |Unpacking Bytes| tutorial for more information.

*************
API Reference
*************

.. autoclass:: Buffer

    .. autoattribute:: offset

    .. automethod:: unpack

    .. automethod:: unpack_and_dump
