.. automodule:: plum.attrdict

.. include:: ../alias.txt

################################
[plum.attrdict] Module Reference
################################

The :mod:`plum.attrdict` module provides the |AttrDictX| transform which
converts bytes into an "attribute accessible" dictionary of values and a
dictionary of values into bytes. The "attribute accessible" dictionary
has normal dictionary behaviors but also offers access to the members of
the dictionary using Python attribute syntax.

This reference page shows how to create and use an |AttrDictX| transform for
packing and unpacking bytes, a lighter weight method than a custom |Structure|
subclass. This page also provides API details.

.. contents::
    :local:

The examples shown on this page require the following setup:

    >>> from plum.array import ArrayX
    >>> from plum.attrdict import AttrDictX
    >>> from plum.bigendian import uint8
    >>> from plum.utilities import pack, unpack


*********
Basic Use
*********

The |AttrDictX| transform accepts the following arguments:

    :members: dictionary of member names and formats
    :name: transform name (for representations)

For example:

    >>> revision_x = AttrDictX({"major": uint8, "minor": uint8})

Use the transform to specify a format when using the |pack()| and |unpack()| utility
functions or when using other high level transforms:

    >>> fmt = {"hardware_rev": revision_x, "software_rev": revision_x}
    >>>
    >>> bindata = pack({"hardware_rev": dict(major=1, minor=0), "software_rev": dict(major=7, minor=2)}, fmt)
    >>> bindata.hex()
    '01000702'
    >>>
    >>> revisions = unpack(fmt, bindata)
    >>> revisions
    {'hardware_rev': AttrDict(major=1, minor=0), 'software_rev': AttrDict(major=7, minor=2)}
    >>>
    >>> # AttrDict members accessible by index or by attribute
    >>> hw_rev = revisions["hardware_rev"]
    >>> hw_rev["major"]
    1
    >>> hw_rev.minor
    0
    >>> array_x = ArrayX("list", fmt=revision_x, dims=(2, ))
    >>>
    >>> bindata = pack([dict(major=1, minor=0), dict(major=7, minor=2)], fmt=array_x)
    >>> bindata.hex()
    '01000702'
    >>>
    >>> unpack(array_x, bindata)
    [AttrDict(major=1, minor=0), AttrDict(major=7, minor=2)]


********************
Usage Considerations
********************

- |ItemsX| offers the capability to pack and unpack dictionaries but doesn't
  offer access to unpacked members via attribute syntax.

- |Structure| subclasses facilitate IDE type ahead as well as better static
  code analysis for the members.

- Instantiating an |AttrDictX| is substantially faster than subclassing
  |Structure|.

- |Structure| offers many more features (e.g. automatically sized members,
  member defaults, customized constructors, etc.)


*************
API Reference
*************

.. autoclass:: AttrDictX

    .. autoattribute:: name

    .. autoattribute:: nbytes

    .. automethod:: pack

    .. automethod:: pack_and_dump

    .. automethod:: unpack

    .. automethod:: unpack_and_dump
