.. automodule:: plum.flag

.. include:: ../alias.txt

################################
[plum.flag] Module Reference
################################

The :mod:`plum.flag` module provides the |FlagX| transform which converts an
integer flags enumeration member into bytes and bytes into an integer flags
enumeration member. This reference page demonstrates creating and using a
|FlagX| transform as well as provides API details.

.. contents::
    :local:

The examples shown on this page require the following setup:

    >>> from enum import IntFlag
    >>> from plum.flag import FlagX
    >>> from plum.utilities import pack, unpack
    >>>
    >>> class Color(IntFlag):
    ...     RED = 1
    ...     GREEN = 2
    ...     BLUE = 4
    ...     WHITE = RED | GREEN | BLUE
    ...

***********
Basic Usage
***********

The |FlagX| transform accepts the following arguments:

    :name: transform name (for representations)
    :enum: associated integer flag enumeration class
    :nbytes: format size in bytes (any positive integer)
    :byteorder: ``"big"`` or ``"little"`` (default)

For example:

    >>> color = FlagX("color", Color, nbytes=2, byteorder="big")

Use the transform to specify a format when using the |pack()| and |unpack()| utility
functions or when using other high level transforms. Its advantage over
using a |IntX| transform is that unpacking results in the corresponding
integer flag enumeration member.

    >>> fmt = [color, color]
    >>>
    >>> unpack(fmt, b'\x00\x01\x00\x06')
    [<Color.RED: 1>, <Color.BLUE|GREEN: 6>]
    >>>
    >>> pack([Color.RED, Color.BLUE|Color.GREEN], fmt)
    b'\x00\x01\x00\x06'

.. Tip::
    Use ``sys.byteorder`` as the |FlagX| transform ``byteorder`` argument to
    get the same byte order as the architecture of the machine your script
    is running on.

*************
API Reference
*************

.. autoclass:: FlagX

    .. autoattribute:: byteorder

    .. autoattribute:: enum

    .. autoattribute:: name

    .. autoattribute:: nbytes

    .. automethod:: pack

    .. automethod:: pack_and_dump

    .. automethod:: unpack

    .. automethod:: unpack_and_dump
