.. include:: ../alias.txt

.. automodule:: plum.structure

#################################
[plum.structure] Module Reference
#################################

This module provides classes for defining a custom data store type that also
acts as a bytes transform to convert data within a data store instance into
bytes and to convert bytes back into a data store instance. Similar to
Python's :mod:`dataclasses`, member properties added to a class definition
control the generation of special methods (e.g. ``__init__()``, ``__repr__``,
etc.) to avoid the need to write that code by hand.

.. contents::
    :local:

The examples shown on this page require the following setup:

    >>> from plum.bigendian import uint8
    >>> from plum.structure import Structure, member
    >>> from plum.structure import dimmed_member, sized_member
    >>> from plum.utilities import pack, unpack

***********
Basic Usage
***********

To create a data store type, subclass the |Structure| class and add
structure data members as class attributes in the class definition
with the |Member| class to define each member's format. The ``fmt``
parameter accepts anything that supports the :mod:`plum` bytes
transformation API (e.g. transform instances or even a data store
class such as another structure). For example:

    >>> class MyStruct(Structure):
    ...     m1: int = member(fmt=uint8)
    ...     m2: int = member(fmt=uint8)
    ...

You may include other class attributes or methods in the class
definition. The |Structure| metaclass which generates the
initializer pays attention only to the class attributes that are a
|Member| (or a subclass of it). The generated initializer accepts
keyword only argument values for each structure member. Access
structure member values as an attribute or by indexing.

    >>> struct = MyStruct(m1=1, m2=2)
    >>> struct
    MyStruct(m1=1, m2=2)
    >>> struct.m1
    1
    >>> struct[0]
    1

Then use the structure class to specify a format when using :func:`pack`
and :func:`unpack` utility functions or other applications where a
format is needed:

    >>> fmt = [uint8, MyStruct]
    >>>
    >>> pack([0, MyStruct(m1=1, m2=2)], fmt)
    b'\x00\x01\x02'
    >>>
    >>> unpack(fmt, b'\x00\x01\x02')
    [0, MyStruct(m1=1, m2=2)]

Since structure data store types support the :mod:`plum` bytes transformation
API, packing a structure instance does not require a format to be specified:

    >>> pack(MyStruct(m1=1, m2=2))
    b'\x01\x02'

The structure data store type accepts any iterable of member values during
packing. The following example packs a simple list:

    >>> pack([1, 2], fmt=MyStruct)
    b'\x01\x02'

**************
Member Default
**************

Use the ``default`` argument in the member definition to specify a default
value for the member. Instantiations use the default value for the member
when one was not provided:

    >>> class MyStruct(Structure):
    ...     m1: int = member(fmt=uint8)
    ...     m2: int = member(fmt=uint8, default=2)
    ...
    >>> struct = MyStruct(m1=1)
    >>> struct
    MyStruct(m1=1, m2=2)
    >>>
    >>> pack(struct)
    b'\x01\x02'

*************
Ignore Member
*************

To ignore certain members when comparing structure instances, use
the ``ignore`` argument in the member definition:

    >>> class MyStruct(Structure):
    ...     m1: int = member(fmt=uint8)
    ...     m2: int = member(fmt=uint8, ignore=True)
    ...
    >>> struct_a = MyStruct(m1=1, m2=0)
    >>> struct_b = MyStruct(m1=1, m2=99)
    >>>
    >>> struct_a == struct_b
    True

To guarantee an exact comparison, compare against a list or use the
:meth:`~plum.Structure.asdict` method:

    >>> struct_a == list(struct_b)
    False
    >>>
    >>> struct_a.asdict() == struct_b.asdict()
    False

****************
Read Only Member
****************

To block against changing the value of a structure member when setting
the structure member attribute, pass ``readonly=True`` when defining
the member property:

    >>> class MyStruct(Structure):
    ...     m1: int = member(fmt=uint8)
    ...     m2: int = member(fmt=uint8, readonly=True)
    ...
    >>> struct = MyStruct(m1=1, m2=2)
    >>> try:
    ...     struct.m2 = 99
    ... except AttributeError:
    ...     print("AttributeError: can't set attribute 'm2'")
    ...
    AttributeError: can't set attribute 'm2'

*****************
Advanced Features
*****************

The following tutorials detail additional structure features:

    ====================================   ================================================
    Tutorial                               Description
    ====================================   ================================================
    |Bit field Structure Member|           comprises partial bytes
    |Dimensioned Array Structure Member|   array member dims controlled by separate dims member
    |Dynamically Typed Structure Member|   member type controlled by separate type member
    |Sized Structure Member|               member size controlled by separate size member
    ====================================   ================================================

*************
API Reference
*************

.. autoclass:: Structure

    .. autoattribute:: byteorder

    .. autoattribute:: dump

    .. autoattribute:: nbytes

    .. automethod:: asdict

    .. automethod:: pack

    .. automethod:: pack_and_dump

    .. automethod:: unpack

    .. automethod:: unpack_and_dump

.. autofunction:: bitfield_member

.. autofunction:: dimmed_member

.. autofunction:: member

.. autofunction:: sized_member

