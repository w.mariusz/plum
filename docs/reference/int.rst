.. automodule:: plum.int

.. include:: ../alias.txt

###########################
[plum.int] Module Reference
###########################

The :mod:`plum.int` module provides the |IntX| transform for converting
an integer number into bytes and bytes into an integer number. This reference
page demonstrates creating and using a |IntX| transform as well as
provides API details.

.. contents::
    :local:

The examples shown on this page require the following setup:

    >>> from plum.array import ArrayX
    >>> from plum.int import IntX
    >>> from plum.utilities import pack, unpack


*********
Basic Use
*********

The |IntX| transform accepts the following arguments:

    :name: transform name (for representations)
    :nbytes: format size in bytes (any positive integer)
    :byteorder: ``"big"`` or ``"little"`` (default)
    :signed: ``True`` or ``False`` (default)

For example:

    >>> uint8 = IntX("uint8", nbytes=1)
    >>> sint16 = IntX("sint16", nbytes=2, byteorder="big", signed=True)

Use the transform to specify a format when using the |pack()| and |unpack()| utility
functions or when using other high level transforms:

    >>> fmt = [uint8, sint16]
    >>>
    >>> bindata = pack([1, -2], fmt)
    >>> bindata.hex()
    '01fffe'
    >>>
    >>> unpack(fmt, bindata)
    [1, -2]
    >>>
    >>> array2x2 = ArrayX("array2x2", fmt=uint8, dims=(2, 2))
    >>>
    >>> bindata = pack([[1, 2], [3, 4]], fmt=array2x2)
    >>> bindata.hex()
    '01020304'
    >>>
    >>> unpack(array2x2, bindata)
    [[1, 2], [3, 4]]

.. Tip::
    Use ``sys.byteorder`` as the |IntX| transform ``byteorder`` argument to
    get the same byte order as the architecture of the machine your script
    is running on.


*************
API Reference
*************

.. autoclass:: IntX

    .. autoattribute:: byteorder

    .. autoattribute:: name

    .. autoattribute:: nbytes

    .. autoattribute:: signed

    .. automethod:: pack

    .. automethod:: pack_and_dump

    .. automethod:: unpack

    .. automethod:: unpack_and_dump
