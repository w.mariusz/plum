.. include:: ../alias.txt

.. automodule:: plum.none

############################
[plum.none] Module Reference
############################

The :mod:`plum.none` module provides the |NoneX| transform for converting
``None`` into no bytes and no bytes into None. This reference page demonstrates
creating and using the |NoneX| transform with a typical use case as well as
provides API details.

.. contents::
    :local:

The examples shown on this page require the following setup:

    >>> from plum.int import IntX
    >>> from plum.none import NoneX
    >>> from plum.structure import Structure, member
    >>>
    >>> uint8 = IntX("uint8", nbytes=1, byteorder="little", signed=False)
    >>> uint16 = IntX("uint16", nbytes=2, byteorder="little", signed=False)

*********
Basic Use
*********

The |NoneX| transform accepts the following argument:

    :name: transform name (for representations)

For example:

    >>> nil = NoneX("nil")

One application where |NoneX| may become necessary is when a structure member's
datatype depends on another structure member. The variably typed member
as shown in the following example demonstrates a use case. The ``data`` member's
type depends on the value of the ``datatype`` member as defined by the type
mapping. When ``datatype`` equals ``0``, no bytes are expected for the ``data``
member. When ``datatype`` equals ``1`` or ``2``, bytes for a ``uint8`` or
``uint16`` (respectively) are expected for the ``data`` member:

    >>> class Struct(Structure):
    ...     datatype: int = member(fmt=uint8)
    ...     data: object = member(fmt={0: nil, 1: uint8, 2: uint16}.__getitem__, fmt_arg=datatype)
    ...
    >>> Struct.unpack(b'\x00').dump()
    +--------+----------+-------+-------+--------+
    | Offset | Access   | Value | Bytes | Format |
    +--------+----------+-------+-------+--------+
    |        |          |       |       | Struct |
    | 0      | datatype | 0     | 00    | uint8  |
    |        | data     | None  |       | nil    |
    +--------+----------+-------+-------+--------+
    >>>
    >>> Struct.unpack(b'\x01\x02').dump()
    +--------+----------+-------+-------+--------+
    | Offset | Access   | Value | Bytes | Format |
    +--------+----------+-------+-------+--------+
    |        |          |       |       | Struct |
    | 0      | datatype | 1     | 01    | uint8  |
    | 1      | data     | 2     | 02    | uint8  |
    +--------+----------+-------+-------+--------+
    >>>
    >>> buffer, dump = Struct(datatype=0, data=None).ipack_and_dump()
    >>> buffer
    b'\x00'
    >>> print(dump)
    +--------+----------+-------+-------+--------+
    | Offset | Access   | Value | Bytes | Format |
    +--------+----------+-------+-------+--------+
    |        |          |       |       | Struct |
    | 0      | datatype | 0     | 00    | uint8  |
    |        | data     | None  |       | nil    |
    +--------+----------+-------+-------+--------+

*************
API Reference
*************

.. autoclass:: NoneX

    .. autoattribute:: name

    .. autoattribute:: nbytes

    .. automethod:: pack

    .. automethod:: pack_and_dump

    .. automethod:: unpack

    .. automethod:: unpack_and_dump
